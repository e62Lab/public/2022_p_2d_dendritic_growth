#include <medusa/Medusa.hpp>
#include "helpers/SplineShape.h"
#include "dendrite/Dendrite.hpp"

using namespace mm;
using namespace std;

void evaluateRecon(HDF& hdf, const DomainDiscretization<Vec2d> domain,
                   const std::function<Vec2d(Vec<double, 1>)> example_r,
                   const std::function<Vec2d(Vec<double, 1>)> der_example_r,
                   const int spline_degree) {
    Eigen::VectorXd angles(domain.size());
    Eigen::VectorXd err_pos(domain.size());
    Eigen::VectorXd err_normal_angle(domain.size());
    for (int i = 0; i < domain.size(); ++i) {
        auto pos = domain.pos(i);
        double angle = std::atan2(pos(1), pos(0));
        //        angle = (angle < 0) ? 2 * mm::PI + angle : angle; // Map to 0->2PI
        angles[i] = angle;

        // ERROR by position.
        auto pos_analytic = example_r(Vec1d{angle});
        auto diff = (pos - pos_analytic).squaredNorm();
        err_pos[i] = diff;

        // ERROR by normal vector angle.
        auto normal = domain.normal(i);
        double normal_angle = std::atan2(normal(1), normal(0));
        //        normal_angle = (normal_angle < 0) ? 2 * mm::PI + normal_angle : normal_angle;//
        //        Map to 0->2PI
        normal_angle *= 180 / mm::PI;

        auto analytic_tangent = der_example_r(Vec1d{angle});
        analytic_tangent.normalize();
        auto analytic_normal = Vec2d{analytic_tangent(1), -analytic_tangent(0)};

        double analytic_normal_angle = std::atan2(analytic_normal(1), analytic_normal(0));
        //        analytic_normal_angle = (analytic_normal_angle < 0) ? 2 * mm::PI +
        //        analytic_normal_angle
        //                                                            : analytic_normal_angle;  //
        //                                                            Map to 0->2PI
        analytic_normal_angle *= 180 / mm::PI;

        double angle_diff = abs(normal_angle) - abs(analytic_normal_angle);
        err_normal_angle[i] = angle_diff;
    }

    hdf.atomic().writeEigen(mm::format("domain_spline_%d/error_pos", spline_degree), err_pos);
    hdf.atomic().writeEigen(mm::format("domain_spline_%d/angle", spline_degree), angles);
    hdf.atomic().writeEigen(mm::format("domain_spline_%d/error_angle", spline_degree),
                            err_normal_angle);
}

void evaluateReconGrad(HDF& hdf, const DomainDiscretization<Vec2d> domain,
                       const std::function<Vec2d(Vec<double, 1>)> example_r,
                       const std::function<Vec2d(Vec<double, 1>)> der_example_r) {
    auto dendrite_idxs = domain.types().filter([&](int i) { return i == -2; });
    Eigen::VectorXd angles(dendrite_idxs.size());
    Eigen::VectorXd err_pos(dendrite_idxs.size());
    Eigen::VectorXd err_normal_angle(dendrite_idxs.size());
    for (int c = 0; c < dendrite_idxs.size(); ++c) {
        int i = dendrite_idxs[c];
        auto pos = domain.pos(i);
        double angle = std::atan2(pos(1), pos(0));
        //        angle = (angle < 0) ? 2 * mm::PI + angle : angle; // Map to 0->2PI
        angles[c] = angle;
        // ERROR by position.
        auto pos_analytic = example_r(Vec1d{angle});
        auto diff = (pos - pos_analytic).squaredNorm();
        err_pos[c] = diff;

        // ERROR by normal vector angle.
        auto normal = domain.normal(i);
        double normal_angle = std::atan2(normal(1), normal(0));
        //        normal_angle = (normal_angle < 0) ? 2 * mm::PI + normal_angle : normal_angle;//
        //        Map to 0->2PI
        normal_angle *= 180 / mm::PI;

        auto analytic_tangent = der_example_r(Vec1d{angle});
        analytic_tangent.normalize();
        auto analytic_normal = Vec2d{analytic_tangent(1), -analytic_tangent(0)};

        double analytic_normal_angle = std::atan2(analytic_normal(1), analytic_normal(0));
        //        analytic_normal_angle = (analytic_normal_angle < 0) ? 2 * mm::PI +
        //        analytic_normal_angle
        //                                                            : analytic_normal_angle;  //
        //                                                            Map to 0->2PI
        analytic_normal_angle *= 180 / mm::PI;

        double angle_diff = abs(normal_angle) - abs(analytic_normal_angle);
        err_normal_angle[c] = angle_diff;
    }

    hdf.atomic().writeEigen("domain/error_pos", err_pos);
    hdf.atomic().writeEigen("domain/angle", angles);
    hdf.atomic().writeEigen("domain/error_angle", err_normal_angle);
}

void build_complete_domain(const XML& conf, DomainDiscretization<Vec2d>& domain,
                           DomainDiscretization<Vec2d>& spline_domain) {
    // Dendrite domain.
    auto dendrite_h = conf.get<double>("domain.dendrite.h");
    auto liquid_h = conf.get<double>("domain.liquid.h");
    auto dendrite_type = conf.get<int>("domain.dendrite.type");
    auto liquid_type = conf.get<int>("domain.liquid.type");
    double liquid_size = conf.get<double>("domain.liquid.size");
    int seed = conf.get<int>("domain.seed");
    if (seed == -1) {
        seed = get_seed();
    }

    // Liquid domain.
    BoxShape<Vec2d> box_liquid(-liquid_size, liquid_size);
    domain = box_liquid.discretizeBoundaryWithStep(liquid_h, liquid_type);
    KDTree<Vec2d> test_tree(domain.positions());
    for (int i : domain.boundary()) {
        domain.type(i) = conf.get<int>("domain.liquid.type");
    }

    // Difference.
    for (int i = 0; i < spline_domain.size(); ++i) {
        spline_domain.type(i) = dendrite_type;
    }
    domain -= spline_domain;

    // Fill domain.
    auto dendrite_idxs = domain.types().filter([&](int i) { return i == dendrite_type; });
    auto spline_points = domain.positions()[dendrite_idxs];
    KDTree<Vec2d> tree(spline_points);
    auto fn = [&](const Vec2d& p) { return dx(p, tree, test_tree, conf); };
    // Fill engine.
    GeneralFill<Vec2d> fill;
    fill.numSamples(conf.get<int>("domain.fill_samples")).seed(seed);
    // Fill domain with nodes.
    domain.fill(fill, fn);

    // Flip normals.
    for (long unsigned int i = 0; i < dendrite_idxs.size(); ++i) {
        domain.normal(dendrite_idxs[i]) = -domain.normal(dendrite_idxs[i]);
    }
}

void revert_and_normalize(DomainDiscretization<Vec2d>& domain) {
    for (int i = 0; i < domain.size(); ++i) {
        auto normal = domain.normal(i);
        normal.normalize();
        domain.normal(i) = -normal;
    }
}

int main(int argc, char* argv[]) {
    // Check for settings file.
    assert_msg(argc >= 2, "Second argument should be the XML parameter file.");

    // Read input config.
    cout << "Reading params from: " << argv[1] << endl;
    const XML conf(argv[1]);

    // Create H5 file to store output.
    string output_file = conf.get<string>("meta.directory_out") +
                         (conf.get<bool>("domain.circle") ? "circle" : "flower") + "_" +
                         conf.get<string>("meta.filename_out") + ".h5";
    cout << "Creating results file: " << output_file << endl;
    HDF hdf(output_file, HDF::DESTROY);

    // Write params to results file.
    hdf.writeXML("conf", conf);
    hdf.close();

    // *******
    // Domain.
    // *******
    std::function<Vec2d(Vec<double, 1>)> example_r;
    std::function<Vec2d(Vec<double, 1>)> der_example_r;

    if (conf.get<bool>("domain.circle")) {
        example_r = [](Vec<double, 1> t) {
            double r = 1;

            return Vec2d(r * cos(t(0)), r * sin(t(0)));
        };

        der_example_r = [](Vec<double, 1> t) {
            double r = 1;
            Eigen::Matrix<double, 2, 1> jm;
            jm.col(0) << -r * sin(t(0)), r * cos(t(0));

            return jm;
        };
    } else {
        example_r = [](Vec<double, 1> t) {
            double r = pow(abs(cos(1.5 * t(0))), sin(3 * t(0)));
            return Vec2d(r * cos(t(0)), r * sin(t(0)));
        };

        der_example_r = [](Vec<double, 1> t) {
            double r = pow(abs(cos(1.5 * t(0))), sin(3 * t(0)));
            double der_r =
                (-1.5 * pow(abs(cos(1.5 * t(0))), sin(3 * t(0))) * sin(3 * t(0)) * sin(1.5 * t(0)) +
                 3 * pow(abs(cos(1.5 * t(0))), sin(3 * t(0))) * cos(3 * t(0)) * cos(1.5 * t(0)) *
                     log(abs(cos(1.5 * t(0))))) /
                cos(1.5 * t(0));

            Eigen::Matrix<double, 2, 1> jm;
            jm.col(0) << der_r * cos(t(0)) - r * sin(t(0)), der_r * sin(t(0)) + r * cos(t(0));

            return jm;
        };
    }

    // Define parametric curve's domain.
    BoxShape<Vec1d> param_bs(Vec<double, 1>{0.0}, Vec<double, 1>{2 * mm::PI});

    UnknownShape<Vec2d> shape;
    DomainDiscretization<Vec2d> domain(shape);  // Acutal domain for fitting.
    auto nodal_spacing = [=](Vec2d p) { return conf.get<double>("domain.dendrite.h"); };

    // Discretize inner bnd.
    GeneralSurfaceFill<Vec2d, Vec1d> gsf;
    gsf.seed(conf.get<int>("domain.seed"));
    domain.fill(gsf, param_bs, example_r, der_example_r, nodal_spacing);

    // *****************
    // Normals by spline
    // *****************
    // Reconstruction with spline degree = 3.
    SplineShape<Vec2d, 3> spline_shape(domain.positions(), conf.get<int>("domain.seed"));
    spline_shape.setInterior({0, 0}, true);
    DomainDiscretization<Vec2d> spline_domain =
        spline_shape.discretizeBoundaryWithStep(conf.get<double>("domain.dendrite.h"), -5);
    revert_and_normalize(spline_domain);
    // Save refined domain.
    hdf.atomic().writeDomain(mm::format("domain_spline_%d", 3), spline_domain);
    // Evaulate reconstruction accuracy.
    evaluateRecon(hdf, spline_domain, example_r, der_example_r, 3);

    // *******************
    // Normals by gradient
    // *******************
    build_complete_domain(conf, domain, spline_domain);
    hdf.atomic().writeDomain("domain", domain);

    // Compute normals.
    auto dir = domain.types() == conf.get<int>("domain.dendrite.type");
    auto neu = domain.types() == conf.get<int>("domain.liquid.type");
    // For interior and Dirichlet boundary.
    domain.findSupport(mm::FindClosest(12).forNodes(domain.interior() + dir));
    // For Neumann boundary.
    domain.findSupport(
        mm::FindClosest(12).forNodes(neu).searchAmong(domain.interior()).forceSelf(true));

    // Compute shapes.
    std::tuple<mm::Lap<2>, mm::Der1s<2>> operators;
    mm::RaggedShapeStorage<Vec2d, decltype(operators)> storage;
    storage.resize(domain.supportSizes());
    // Compute shapes.
    Polyharmonic<double, 3> ph;
    using MatrixType = Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic>;
    RBFFD<decltype(ph), Vec2d, ScaleToClosest, Eigen::PartialPivLU<MatrixType>> appr(
        ph, Monomials<Vec2d>(2));
    mm::computeShapes(domain, appr, domain.all(), operators, &storage);

    // Operators.
    auto op = storage.explicitOperators();

    // Define field.
    auto dendrite_indexes = domain.types() == conf.get<int>("domain.dendrite.type");
    Eigen::VectorXd u(domain.size());
    u.setOnes();
    for (int i : dendrite_indexes) {
        u(i) = 0;
    }

    // Compute grad normals.
    Range<Vec2d> grad_normals(domain.size());
    for (int j : dendrite_indexes) {
        Vec2d grad = op.grad(u, j);
        grad_normals[j] = grad.normalized();
        domain.normal(j) = grad_normals[j];
    }

    hdf.atomic().writeDouble2DArray("domain/grad_normals", grad_normals);

    // evaluate grad normals.
    evaluateReconGrad(hdf, domain, example_r, der_example_r);

    // Complete.
    return 0;
}
