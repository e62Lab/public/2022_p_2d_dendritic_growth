import sys
import subprocess
import os
import time
import glob

# Run parameters.
command = sys.argv[1]
xmls_dir = sys.argv[2]
max_processes = int(sys.argv[3])

# Input files.
files = glob.glob(xmls_dir + '*.xml')

# Initilaize parallel run.
processes = set()
for name in files:
    processes.add(subprocess.Popen([command, xmls_dir + os.path.basename(
        name)], stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL))
    if len(processes) >= max_processes:
        os.wait()
        processes.difference_update(
            [p for p in processes if p.poll() is not None])

# Check if all the child processes were closed.
for p in processes:
    if p.poll() is None:
        p.wait()

print('Script is done.')
