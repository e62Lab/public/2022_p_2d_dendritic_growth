cmake_minimum_required(VERSION 3.10)
project(envelope_growth)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/bin) # set bin

set(CMAKE_CXX_STANDARD 17)
add_compile_options(-Wall -O3 -fopenmp)

set(CMAKE_BUILD_TYPE Release)
#set(CMAKE_BUILD_TYPE Debug)

add_subdirectory($ENV{HOME}/medusa medusa)
include_directories($ENV{HOME}/medusa/include/)

foreach(EXECUTABLE phase-change reconstruction interpolation interpolation_conv spline_order refine reconstruction_accuracy normal_analysis)
    add_executable(${EXECUTABLE} source/${EXECUTABLE}.cpp)
    target_link_libraries(${EXECUTABLE} medusa)
    target_link_libraries(${EXECUTABLE} gomp)
endforeach()
