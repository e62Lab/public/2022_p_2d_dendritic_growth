\section{Conclusions and perspectives}
\label{sec:conclusions}

In this paper we proposed the Meshless Interface Tracking (MIT) method for the solution of the Grain Envelope Model for dendritic growth (GEM). This novel method is expected to provide improved accuracy of GEM solutions in comparison with the Phase-Field Interface Capturing (PFIC) method, which has been used in all prior work on the GEM. Inherently, the MIT-GEM eliminates certain numerical artefacts experienced with the PFIC-GEM:
\begin{itemize}
	\item The MIT-GEM does not smooth the envelope, while smoothing cannot be entirely avoided with the PFIC-GEM.
	\item The MIT-GEM ensures that the envelope propagates exactly at the calculated speed, while the PFIC-GEM requires a careful surface reconstruction to ensure good accuracy of the envelope propagation.
\end{itemize}
Furthermore, the MIT introduces \h-adaptive spatial discretization, which is an advantage and enables one to ensure high accuracy of the solution of the diffusion field in the liquid. In the future it could also be used to refine the point spacing on the envelope in areas of high curvature. In addition to $h$ also an $hp$ adaptivity could be used~\cite{jancic_strong_2023}. 
\hl{
We analysed all critical elements of the MIT method, namely the meshless approximation of the differential operators, the interpolation of the concentration field, and the parametric envelope reconstruction used for rediscretization and for calculation of the envelope speed. We addressed the accuracy and computational complexity of the methods as well as the sensitivity to the order of RBF operator approximation and to the order of spline interpolation for envelope reconstruction. After the analysis we concluded that the order $k > 1$ should be used for surface reconstruction, and the second order RBF-FD approximation should be used for field reconstruction and its derivation. Note that these are not tuning parameters, but parameters that define the method (similar to the FEM, where the user decides whether to use linear basis functions or quadratic basis functions). Therefore, these parameters can be considered universal.
}

The comparison of PFIC and MIT shows that MIT can capture the shape of the envelope more accurately by using fewer nodes on the envelope. Moreover, due to the \h-refinement, the total number of nodes required in MIT is also much smaller compared to PFIC. MIT reaches high accuracy using second order methods for the approximation of the spatial partial differential operators, spatial field interpolation, and for spline reconstruction of the envelope. Higher order methods can be readily used for additional improvement of accuracy.

The focus of future applications of the MIT-GEM will be simulations requiring high accuracy, especially high accuracy of the envelope tracking. This includes simulations where the development of new branches of a dendrite is a key aspect. The MIT-GEM will also serve as a high-accuracy reference for the verification of the 
%computationally cheaper 
PFIC-GEM. From the numerical point of view, the introduction of a more complex node distribution that adapts to the local envelope curvature radius would be beneficial to improve the description of small-scale features of the envelope shape.

\hl{
The presented MIT is based on a meshless approximation using scattered nodes for spatial discretisation. Despite the 
%romantic 
often idealized
descriptions of meshless methods found in literature, there are still many unanswered questions that burden the method. The ongoing scientific debate about what is the best nodal distribution is inherently connected to the subsequent challenge of stencil selection, where scientific consensus is even more elusive. In MIT we use a widely accepted stencil that comprises the certain number of nearest nodes. Although such an approach is simple to implement and generalises to higher dimensions, it is also computationally expensive as relatively big stencils are required for stable approximation}~\cite{bayona2019insight, bayona2010rbf, bayona2017role}. 
\hl{The interplay of node positioning and stencil selection is of crucial importance not only for the execution performance but also for accuracy and stability, which is still, after decades of research, not fully understood}~\cite{kolar2023}. \hl{In the future work these concerns should be also tackled. In addition to the simple closest nodes stencils, there is promising research}~\cite{davydov2011adaptive,davydov2022improved}\hl{ showing that more sophisticated symmetric stencils could improve the stability and execution performance of the meshless methods. 

The introduction of scattered nodes also results in an unstructured nature of the data, which makes load balancing in parallel} execution~\cite{trobec_parallel_2015} and effective cache utilisation much more difficult~\cite{kosec_super_2014}, \hl{opening additional questions regarding the effective implementation of meshless methods, which could also be addressed in future work.}
