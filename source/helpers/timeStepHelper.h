#ifndef ENVELOPE_GROWTH_TIMESTEPHELPER_H
#define ENVELOPE_GROWTH_TIMESTEPHELPER_H

#include <medusa/Medusa.hpp>

double getTimeStep(const mm::XML& conf) {
    return mm::ipow<2>(conf.get<double>("domain.dendrite.h")) * 0.5 * conf.get<double>("simulation.dt_factor");
}
#endif  // ENVELOPE_GROWTH_TIMESTEPHELPER_H
