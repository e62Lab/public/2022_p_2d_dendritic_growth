# Scripts

To run with CPU affinity, we use [taskset](https://linux.die.net/man/1/taskset).

```shell
taskset -c 0 simulation.x
```

for example

```shell
# Loop over all files.
for i in {000000..000014}; do
  echo "Running on input: ../input/generated/settings_${i}.xml"
  taskset -c 0 ./phase-change ../input/generated/settings_${i}.xml >>/dev/null
done
```