
# A sharp-interface mesoscopic model for two-dimensional dendritic growth

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/e62Lab%2Fpublic%2F2022_p_2d_dendritic_growth/main)

Scientific paper where simulation of 2D dendritic growth using meshless methods is presented.

The project is closely related to

- [this repository](https://gitlab.com/e62Lab/phase-change/) and
- [this repository](https://gitlab.com/e62Lab/public/2022_cp_icasp_dendrite_growth)

## Abstract

The growth of dendritic grains during solidification is often modelled using the Grain Envelope Model (GEM), in which the envelope of the dendrite is an interface tracked by the Phase Field Interface Capturing (PFIC) method. In the PFIC method, an phase-field equation is solved on a fixed mesh to track the position of the envelope. While being versatile and robust, PFIC introduces certain numerical artefacts. In this work, we present an alternative approach for the solution of the GEM that employs a Meshless (sharp) Interface Tracking (MIT) formulation, which uses direct, artefact-free interface tracking. In the MIT, the envelope (interface) is defined as a moving domain boundary and the interface-tracking nodes are boundary nodes for the diffusion problem solved in the domain. To increase the accuracy of the method for the diffusion-controlled moving-boundary problem, an \h-adaptive spatial discretization is used, thus, the node spacing is refined in the vicinity of the envelope. MIT combines a parametric surface reconstruction, a mesh-free discretization of the parametric surfaces and the space enclosed by them, and a high-order approximation of the partial differential operators and of the solute concentration field using radial basis functions augmented with monomials. The proposed method is demonstrated on a two-dimensional \h-adaptive solution of the diffusive growth of dendrite and evaluated by comparing the results to the PFIC approach. It is shown that MIT can reproduce the results calculated with PFIC, that it is convergent and that it can capture more details in the envelope shape than PFIC with a similar spatial discretization.

## Visuals

All visuals are in the `manuscript/paper/figures` directory. It might also be worth to check the `logs/`.

## Installation

Requirements:

- CMake
- C++ compiler
- Python 3.9 (or higher)
- Jupyter notebooks
- libboost_dev
- [Medusa](https://e6.ijs.si/medusa/) C++ library

## Usage

Create or go to `build/` directory and build using

```bash
cmake .. && make -j 12
```

All executables will be created in `bin/` directory. The executables must be run with a parameter with all the settings, e.g.

```bash
./phase-change ../input/solo.xml
```

## Support

Thanks to [E62 team](https://e6.ijs.si/parlab/) and **Miha Založnik** and **Miha Rot** for support.

## Authors and contributors

- **Mitja Jančič** under supervision of
- **Miha Založnik** and
- **Gregor Kosec**

## Acknowledgments

M.J. and G.K. acknowledge the financial support from the Slovenian Research Agency research core funding No. P2-0095 and research project No. J2-3048. M.Z. acknowledges the support by the French State through the program ``Investment in the future'' operated by the National Research Agency (ANR) and referenced by ANR-11 LABX-0008-01 (LabEx DAMAS). A part of the required high performance computing resources was provided by the EXPLOR center hosted by the Université de Lorraine.

# Reproducing paper results

## Single discretization

You can use the `root/input/test.xml` for the input file. Change the parameters if required and run command

```bash
./phase-change ../input/test.xml
```

## Multiple discretizations

To run for multiple discretizations, modify the general parameters in `root/input/test.xml`. Then navigate to `root/scripts/`and run the python script

```bash
python3 generate_xmls.py
```

This will generate all input files and store them in `root/input/generated/` directory.

To run all of them at once (for example on server) with parallel execution, you can simply run the

```bash
python3 par_run.py ./../bin/phase-change ../input/generated 20
```

Or if you wish to measure the times, simply run the attached bash script (do not forget to fix the execution frequency beforehand)

```bash
./run_for_time_measurements.sh
```

> **Warning**: Simulation can take a while.

## Data

Raw data is available from Zenodo: [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.8386292.svg)](https://doi.org/10.5281/zenodo.8386292)