#ifndef PHASE_CHANGE_COMPUTE_SHAPES_H
#define PHASE_CHANGE_COMPUTE_SHAPES_H

#include <medusa/Medusa.hpp>

namespace mj {
template <typename vec_t, typename approx_t_rbffd>
mm::RaggedShapeStorage<vec_t, std::tuple<mm::Lap<vec_t::dim>, mm::Der1s<vec_t::dim>>> computeShapes(
    mm::DomainDiscretization<vec_t>& domain, const mm::XML& conf, approx_t_rbffd rbffd_appr) {
    // Operators.
    std::tuple<mm::Lap<vec_t::dim>, mm::Der1s<vec_t::dim>> operators;
    mm::RaggedShapeStorage<vec_t, decltype(operators)> storage;
    storage.resize(domain.supportSizes());

    // Compute shapes.
    mm::computeShapes(domain, rbffd_appr, domain.all(), operators, &storage);
    return storage;
}

}  // namespace mj
#endif  // PHASE_CHANGE_COMPUTE_SHAPES_H
