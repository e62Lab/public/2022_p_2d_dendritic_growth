#include <medusa/Medusa.hpp>
#include "helpers/SplineShape.h"

using namespace mm;
using namespace std;

template <int spline_degree>
void run_for_circular_growth(const XML& conf, HDF& hdf) {
    BallShape<Vec2d> shape(0, 1);
    const double dx = conf.get<double>("growth.h");
    auto domain = shape.discretizeBoundaryWithStep(dx);
    prn(domain.size());

    double dt = 0.5 * ipow<2>(dx);
    int total_steps = conf.get<int>("growth.time_steps");

    Range<Vec2d> positions = domain.positions();
    for (int step = 0; step <= total_steps; ++step) {
        SplineShape<Vec2d, spline_degree> spline_shape(positions,
                                                       conf.template get<int>("domain.seed"));
        spline_shape.setInterior({0.1, 0.1}, true);
        domain = spline_shape.discretizeBoundaryWithStep(dx, -5);

        positions = domain.positions();

        for (int i = 0; i < domain.size(); ++i) {
            Vec2d normal = -domain.normal(i);
            normal = normal / normal.norm();
            domain.normal(i) = normal;

            Vec2d disp = normal * dt * (conf.template get<double>("growth.v"));
            positions[i] = domain.pos(i) + disp;
        }
        if (step % (total_steps / 10) == 0) {
            cout << (double)step / total_steps * 100 << " %" << endl;

            //            prn(domain.size());
            //            hdf.atomic().writeDomain(mm::format("domain_growth_%05d", step), domain);
        }
    }

    hdf.atomic().writeDomain(mm::format("domain_growth_%d", spline_degree), domain);
    prn(domain.size());
};

int main(int argc, char* argv[]) {
    // Check for settings file.
    assert_msg(argc >= 2, "Second argument should be the XML parameter file.");

    // Timer initialization.
    Timer t;
    t.addCheckPoint("start");

    // Read input config.
    cout << "Reading params from: " << argv[1] << endl;
    const XML conf(argv[1]);

    // Create H5 file to store output.
    string output_file =
        conf.get<string>("meta.directory_out") + conf.get<string>("meta.filename_out") + ".h5";
    cout << "Creating results file: " << output_file << endl;
    HDF hdf(output_file, HDF::DESTROY);

    // Write params to results file.
    hdf.writeXML("conf", conf);
    hdf.close();

    // Domain.
    auto example_r = [](Vec<double, 1> t) {
        double r = pow(abs(cos(1.5 * t(0))), sin(3 * t(0)));
        return Vec2d(r * cos(t(0)), r * sin(t(0)));
    };

    auto der_example_r = [](Vec<double, 1> t) {
        double r = pow(abs(cos(1.5 * t(0))), sin(3 * t(0)));
        double der_r =
            (-1.5 * pow(abs(cos(1.5 * t(0))), sin(3 * t(0))) * sin(3 * t(0)) * sin(1.5 * t(0)) +
             3 * pow(abs(cos(1.5 * t(0))), sin(3 * t(0))) * cos(3 * t(0)) * cos(1.5 * t(0)) *
                 log(abs(cos(1.5 * t(0))))) /
            cos(1.5 * t(0));

        Eigen::Matrix<double, 2, 1> jm;
        jm.col(0) << der_r * cos(t(0)) - r * sin(t(0)), der_r * sin(t(0)) + r * cos(t(0));

        return jm;
    };

    // Define parametric curve's domain.
    BoxShape<Vec1d> param_bs(Vec<double, 1>{0.0}, Vec<double, 1>{2 * PI});

    UnknownShape<Vec2d> shape;
    DomainDiscretization<Vec2d> domain(shape);

    double dx = conf.get<double>("domain.h");
    auto gradient_h = [=](Vec2d p) { return dx; };

    GeneralSurfaceFill<Vec2d, Vec1d> gsf;
    domain.fill(gsf, param_bs, example_r, der_example_r, gradient_h);

    hdf.atomic().writeDomain("domain", domain);
    prn(domain.size());

    // Reconstruction with spline.
    {
        SplineShape<Vec2d, 1> spline_shape(domain.positions(), conf.get<int>("domain.seed"));
        spline_shape.setInterior({0.1, 0.1}, true);
        DomainDiscretization<Vec2d> spline_domain =
            spline_shape.discretizeBoundaryWithStep(conf.get<double>("domain.h_refine"), -5);
        for (int i = 0; i < spline_domain.size(); ++i) {
            spline_domain.normal(i) = -spline_domain.normal(i) / spline_domain.normal(i).norm();
        }
        hdf.atomic().writeDomain(mm::format("domain_spline_%d", 1), spline_domain);
    }
    {
        SplineShape<Vec2d, 2> spline_shape(domain.positions(), conf.get<int>("domain.seed"));
        spline_shape.setInterior({0.1, 0.1}, true);
        DomainDiscretization<Vec2d> spline_domain =
            spline_shape.discretizeBoundaryWithStep(conf.get<double>("domain.h_refine"), -5);
        for (int i = 0; i < spline_domain.size(); ++i) {
            spline_domain.normal(i) = -spline_domain.normal(i) / spline_domain.normal(i).norm();
        }
        hdf.atomic().writeDomain(mm::format("domain_spline_%d", 2), spline_domain);
    }
    {
        SplineShape<Vec2d, 3> spline_shape(domain.positions(), conf.get<int>("domain.seed"));
        spline_shape.setInterior({0.1, 0.1}, true);
        DomainDiscretization<Vec2d> spline_domain =
            spline_shape.discretizeBoundaryWithStep(conf.get<double>("domain.h_refine"), -5);
        for (int i = 0; i < spline_domain.size(); ++i) {
            spline_domain.normal(i) = -spline_domain.normal(i) / spline_domain.normal(i).norm();
        }
        hdf.atomic().writeDomain(mm::format("domain_spline_%d", 3), spline_domain);
    }
    {
        SplineShape<Vec2d, 4> spline_shape(domain.positions(), conf.get<int>("domain.seed"));
        spline_shape.setInterior({0.1, 0.1}, true);
        DomainDiscretization<Vec2d> spline_domain =
            spline_shape.discretizeBoundaryWithStep(conf.get<double>("domain.h_refine"), -5);
        for (int i = 0; i < spline_domain.size(); ++i) {
            spline_domain.normal(i) = -spline_domain.normal(i) / spline_domain.normal(i).norm();
        }
        hdf.atomic().writeDomain(mm::format("domain_spline_%d", 4), spline_domain);
    }

    // SIMULATION
    cout << "Running growth simulation" << endl;
#pragma omp parallel for default(none) shared(conf, hdf) num_threads(5)
    for (int i = 1; i <= 4; ++i) {
        if (i == 1) {
            run_for_circular_growth<1>(conf, hdf);
        } else if (i == 2) {
            run_for_circular_growth<2>(conf, hdf);
        } else if (i == 3) {
            run_for_circular_growth<3>(conf, hdf);
        } else if (i == 4) {
            run_for_circular_growth<4>(conf, hdf);
        }
    }

    // End execution.
    t.addCheckPoint("end");
    prn(t.duration("start", "end"));
    cout << "Calculations saved to: " << output_file << "." << endl;

    // Save timer.
    hdf.atomic().writeTimer("timer", t);

    return 0;
}
