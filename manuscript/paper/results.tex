\section{Simulation of dendritic growth}
\label{sec:results}
%The simulation is started by building the initial domain discretization, where the initial domain is a difference between the square domain with dimensionless side length $L = 20$ representing the outer liquid boundary and the initial grain envelope represented as a circle with dimensionless radius $r_0 = 0.22239$. The node density is prescribed to linearly decrease from $\he$ on the dendrite envelope towards the $h_\ell = 0.1$ on the outer liquid boundary. We use mixed boundary conditions with Neumann boundary condition on the outer liquid boundary ($\Gamma_\ell$) and Dirichlet boundary condition on the dendritic envelope ($\Gamma_\mathrm{e}$) with prescribed solute concentration. The initial solute concentration is homogeneous and equals $\Omega_0 = 0.18$, except on the dendritic envelope where the concentration is constant throughout the simulation and equals 0. A graphical sketch of the problem including boundary conditions is given in Figure~\ref{fig:sketch}. For clarity, the initial dendritic envelope is sketched with a dashed circle. Additionally, a sketch of the linearly \h-refined discretization nodes from the outer liquid boundary towards the envelope is demonstrated with black nodes in the top right corner of the sketch. Note that all our simulations are stopped after the maximum tip position reaches $x_{\text{tip}}=8.9$. Of course, other stopping criteria could be used, but this was sufficient for our analyses.

\begin{wrapfigure}{r}{0.4\textwidth}
	% \vspace{-0.5cm}
	\vspace{-2.5cm}
	\begin{center}
		\includegraphics[width=0.99\linewidth]{figures/sketch.jpg}
		\caption{Schematic representation of dendritic growth problem.}
		\label{fig:sketch}
	\end{center}
\end{wrapfigure}
%In this section, dendritic growth simulations employing the standard PFIC-based and the proposed MIT-based GEM are studied. Their performance in terms of accuracy is analysed.
In this section we apply the MIT to the full GEM model to perform simulations of a dendritic grain and we compare the results to the PFIC method. The dendrite growth problem is schematically shown in Figure~\ref{fig:sketch}. The computational domain is a square of side length $L=20$ with Neumann boundary condition enforced on the outer (liquid) boundary $\Gamma_\ell$ and a Dirichlet boundary condition of $u=0$ enforced on the growing dendrite envelope $\Gamma_\mathrm{e}$. At time $t=0$, the dendrite envelope is initialized with a circle of radius $r_0 = 0.22239$ and the liquid in the domain $\Omega$ has a uniform initial concentration of $u_0=0.18$. The simulations are ran until the primary dendrite tip growing along the $x$ axis reaches position $x_{\text{tip}}=8.9$.
%The time step is a function of node spacing on the envelope $\he$, i.e., $\mathrm{d}t = 0.1 \frac{\he^2}{2}$.

The node spacing on the envelope, $h_\mathrm{e}$ is kept constant throughout the simulation as described in Section~\ref{sec:reconstruction}. In the domain, the inner nodes are generated such that the node spacing increases linearly from $h_\mathrm{e}$ on the dendrite envelope to $h_\ell = 0.1$ on the outer liquid boundary, according to the nodal density function defined in Equation~\eqref{eq:slakNodes}. Note that $h_\ell = 0.1$ is constant and identical for all simulations, regardless of $\he$. We use the RBF-FD approximation method, where the approximation basis consists of cubic PHS $r^3$ and monomials of second, $m=2$, and fourth, $m=4$, degree. In all cases, the differential operator approximation and the local interpolation of the concentration to the stagnant film are performed with the same RBF-FD setup. We present the results for both, i.e., the second and fourth order RBF approximations.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.75\textwidth]{figures/solution_fields.jpg}
	\caption{Growth of the dendritic grain envelope, the concentration field in the liquid and the adaptive discretization for $\he=0.028$}
	\label{fig:solution_fields}
\end{figure}

Figure~\ref{fig:solution_fields} shows the solute concentration fields for selected simulation time steps computed with MIT using the internodal spacing $\he \approx 0.028$ on the dendrite envelope, effectively resulting in $67\,544$ and $77\,168$ discretization nodes at the first and last time steps, respectively. Note that due to the symmetry of the problem, only the first quadrant of the domain $\Omega$ is shown, although the entire dendrite was simulated.

To compare the MIT and PFIC methods, the envelope ($\Gamma_\mathrm{e}$) is compared at different times in Figure~\ref{fig:envelop_shapes}, along with the primary dendrite tip velocity with respect to time $t$ in Figure~\ref{fig:conv}. Although the results of both methods are generally consistent and are very close, there are some differences that are the result of the differences of the two solution approaches. Nevertheless, the first remark can be made: Since PFIC and MIT generally agree, MIT can also be used to resolve the GEM.

%Again, the MIT and PFIC in general agree well-based approach results in numerical solutions that are in good agreement with the PFIC-based approach. The computed dimensionless tip velocity matches well with the reference PFIC-based solution within the entire range of tip positions. Furthermore, convergent behaviour with respect to discretization quality of the dendrite envelope and its neighbourhood can also be observed. The green color is used to denote the coarsest discretization, while the blue color is used to denote the finest discretization.

\begin{figure}[h]
	\centering
	\centering
	\begin{subfigure}[b]{0.47\textwidth}
		\centering
		\includegraphics[width=\textwidth]{figures/example_shapes.eps}
		\caption{Dendritic envelope shape time lapse.}
		\label{fig:envelop_shapes}
	\end{subfigure}
	\hfill
	\begin{subfigure}[b]{0.515\textwidth}
		\centering
		\includegraphics[width=\textwidth]{figures/convergence_max_tip_vs_number_of_nodes.eps}
		\caption{Primary dendrite tip velocity with respect to simulation time.}
		\label{fig:conv}
	\end{subfigure}
	\caption{Comparison of MIT and PFIC on dendritic growth problem for $\he\approx 0.028$.}
	\label{fig:dendritic_envelopes}
\end{figure}



\subsection{Anisotropy induced by the scattered node arrangement}
Although the growth of the dendritic envelope is symmetric according to the model, we can expect irregularities in this respect due to the numerical errors. This is even more pronounced in scattered nodes layout where its numerical anisotropy~\cite{REUTHER201216} additionally distorts the model symmetry. Until now we assumed perfect symmetry and considered only envelope nodes from the first quadrant. Nevertheless, since a full dendrite is simulated we can also assess the level of anisotropy induced by the MIT scheme. For this purpose, the envelope nodes from all four quadrants are compared. On the left plot of Figure~\ref{fig:induced_anisotropy} the comparison of all four quadrants is presented for internodal spacing on the envelope set to $h_e \approx 0.028$ and at simulation time $t\approx 9.4$, i.e., close to the end of simulation, where the branches are well developed. All four envelope parts appear to be in good agreement.

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{figures/quadrants.eps}
	\caption{Analysis of anisotropy induced by the node arrangement. On the left, we show the envelope parts mapped to the first quadrant, on the right the evaluation process is presented.}
	\label{fig:induced_anisotropy}
\end{figure}

For a more objective analysis, a cubic spline is fitted to the mapped nodes of the four envelope parts and the distance to the origin, $r_n(t) = \sqrt{x_n(t)^2 + y_n(t)^2}$, is measured with respect to the spline parameter, $t$, in the top tight corner in Figure~\ref{fig:induced_anisotropy}. Here, parameter $t$ runs from the top of the dendrite tip that grows along direction $\widehat{\b e}_y$ (vertically) towards the tip growing along direction $\widehat{\b e}_x$ (horizontally) and the index $n\in \{I, II, III, IV\}$ denotes the quadrants. The spline presentation allows us to evaluate the differences between the four curves, i.e., the four envelope parts from different quadrants.

The differences with respect to the envelope shape from the first quadrant, represented as a set of 1000 $(x_I, y_I)$ coordinates, are shown in Figure~\ref{fig:induced_anisotropy} in the bottom right. In this particular example, the largest difference is observed in the second quadrant, with the largest relative error slightly above $ 1\%$. The relative error in other quadrants is similar. The largest error, $e_\mathrm{max} = \max|(r_I-r_n)/r_I|$, is approximately 3 times the internodal distance enforced at the envelope, i.e., $e_\mathrm{max}/h_e \approx 3$. This may appear a lot, but note that it is only around $0.8\%$ of the grain size (given by the tip position $x_\mathrm{tip} \approx 8.88$).

%STo sum up, the anisotropy induced by the node arrangement is present, however, based on this analysis, we conclude that the contribution can be neglected.


\subsection{Spatial-discretization convergence}
Let us now take a closer look at the dendritic envelopes. To assess the behaviour of the numerical error in spatial discretization, we compare the envelopes computed with three different spatial discretizations at time $t\approx 9.6$, which corresponds to nearly the end of simulation time.

Figure~\ref{fig:conv_shape} shows the results for the finest ($\he \approx 0.028$), intermediate ($\he \approx 0.060$), and coarsest ($\he \approx 0.1$) using the proposed MIT-based approach and the results for the finest ($h \approx 0.02$), intermediate ($h \approx 0.05$), and coarsest ($h \approx 0.1$) discretizations using the PFIC-based approach. Note that the mesh spacing, $h$, of the PFIC is uniform and equal to $\he$ and that for the MIT, the pscing on the outer boundary, $h_\ell = 0.1$, is constant and identical for all simulations, regardless of $\he$.

\begin{figure}[h]
	\centering
	\centering
	\includegraphics[width=0.8\textwidth]{figures/convergence_envelope_shape.eps}
	\caption{Convergence of the envelope shape at a selected simulation time $t\approx 9.6$. In the bottom row, the centre of the dendrite is shown on the left while the tip shape is shown on the right. \hl{For clarity, only every 20th node is plotted for the PFIC method.}}
	\label{fig:conv_shape}
\end{figure}

Convergent behaviour of the maximum tip position can be already seen in Figure~\ref{fig:conv_shape} at the bottom right figure.
%The maximum tip position computed with MIT is generally increasing with the number of the nodes on the envelope, while on the contrary the maximum tip positions computed with the PFIC-based approach decreases with respect to the quality of the domain discretization. 
To further support this observation, the maximum tip position is plotted with respect to the discretization spacing in Figure~\ref{fig:conv_envelope_length_pos} (left) near the end of the simulation. Here we can see that the slope of convergence of the MIT-based approach is steeper than that of PFIC and also that MIT reaches the asymptotic value at somewhat larger node spacing than PFIC. The PFIC-based approach converges towards slightly larger tip positions than those obtained with the MIT-based simulation. Nevertheless, the difference between the two is less than 0.5 \%, which is small, considering that we are comparing two conceptually different methods. We also show that increasing the order of the RBF approximation from second ($m=2$) to fourth ($m=4$) has only negligible impact on the solution of the full GEM. 

In the middle of the dendrite (see Figure~\ref{fig:conv_shape} in the lower left corner), we notice that MIT captures the final envelope shape with fewer nodes compared to PFIC. Note that PFIC with $\he \approx 0.1$ completely smooths out the depression in the centre, while MIT with a comparable spatial discretization captures all the details. The most likely reason for this is that MIT uses sharp interface tracking and avoids the smoothing effect of the phase field in curved parts of the shape, effectively requiring less nodes to reduce numerical artefacts. We attempt to evaluate this effect by measuring the length of the envelope. To do this, we use the spline representation of the dendritic envelope and calculate its total length by summing 5000 linear segments, which is shown in Figure~\ref{fig:conv_envelope_length_pos} (right).

\begin{figure}[H]
	\centering
	\includegraphics[width=0.85\textwidth]{figures/position_and_envelope_length.eps}
	\caption{Maximum tip position with respect to the discretization spacing (left) and envelope contour length (right).}
	\label{fig:conv_envelope_length_pos}
\end{figure}


\subsection{Computational cost}
To show the benefit of \h-adaptive discretization for the GEM we analyse the evolution of the number of discretization nodes during the simulations. Figure~\ref{fig:node_count_at_time} shows the total number of nodes (domain and boundary nodes), $N_\mathrm{total}$, with respect to the internodal distance $\he$. The number of nodes is generally much lower with the MIT approach, because in the MIT (i) the interior of the dendrite is not considered as part of the computational domain, and (ii) the $h$-refinement towards the dendrite envelope effectively leads to a lower total number of nodes, while preserving the quality of the local field description near the envelope. The last two remarks are particularly important for fine internodal distances $\he$, which yield about an order of magnitude fewer computational points for the MIT at $\he \approx 0.028$. Figure~\ref{fig:node_count_at_h} shows the evolution of the total number of discretization nodes with respect to the simulation time for the finest discretization used in this work ($\he \approx 0.028$). Given the \h-refinement, we find that the number of discretization points generally increases during the simulation. This is somewhat intuitive since the dendrite area increases with simulation time, i.e., the densely populated area increases. On the other hand, an increasing size of the dendrite also means a decrease in the total domain size, which prevails as the dendrite envelope approaches the outer boundary of the domain.

\begin{figure}[h]
	\centering
	\centering
	\begin{subfigure}[b]{0.47\textwidth}
		\centering
		\includegraphics[width=\textwidth]{figures/conv_at_time.eps}
		\caption{Number of nodes at simulation time $t\approx 9.60$ with respect to the internodal distance $\he$, comparison of MIT and PFIC.}
		\label{fig:node_count_at_time}
	\end{subfigure}
	\hfill
	\begin{subfigure}[b]{0.47\textwidth}
		\centering
		\includegraphics[width=\textwidth]{figures/conv_at_h.eps}
		\caption{Time evolution of the number of nodes for the MIT for $\he=0.028$.}
		\label{fig:node_count_at_h}
	\end{subfigure}
	\caption{Analysis of the number of discretization nodes for \h-adaptive MIT and comparison to PFIC.}
	\label{fig:dendritic_envelopes2}
\end{figure}


%\mz{Using space adaptivity (adaptive meshing or \h-adaptivity) in connection with the PFIC-GEM would allow a comparison of the impact of RBF-solution of diffusion and of the MIT inteface tracking on the performance.
%We did not implement space adaptivity to PFIC -- too complicated.
%We compare non-adaptive MIT to PFIC. This allows a very similar comparison because we omit the $h$-adaptivity from the both methods for the comparison. The comparison thus boils down to the benefit of RBF and of the MIT-interface tracking.
%
%Point 1: compares non-adaptive MIT to PFIC. The cost of the MIT is higher. We can suppose the the cost is:\\
%-decreased because of a smaller number of nodes (only the liquid part is in the domain for the solution of the diffusion equation)\\
%-decreased because it does not need to solve the PF equation for front propagation, but a computationally cheaper motion of markers.\\
%-decreased because of the different front tracking method that requires less evaluations of the front velocity ($O(N)$ instead of $O(9 N)$)\\
%-increased because of the node regeneration in the domain and on the envelope, required by the changing domain shape\\
%-increased because of the costlier RBF solution method for the diffusion equation\\
%The latter factors prevail and the cost of the MIT is around 50\% higher for a uniform node spacing.
%}


\hl{
To assess the computational cost of the MIT-GEM and to compare its performance to the PFIC-GEM we measured execution times in controlled conditions, i.e., on the same hardware setup, ensuring CPU affinity and fixed CPU frequency (2.4GHz). We ran MIT-GEM computations with $h_\ell=0.1$ on the outer boundary and with varying $\he$. The mesh spacing for the PFIC-GEM is uniform and equal to $\he$. Note that we can reasonably consider that the two methods have the same accuracy at equal $\he$. This is a simplification that is not far from reality (see Figure
}
~\ref{fig:conv_envelope_length_pos}). 
\hl{
The dependence of the execution time on $\he$ is shown in Figure
}
~\ref{fig:execution_times}.

\hl{
At $\he=h_\ell=0.1$ the node distribution of the MIT is uniform and this point therefore allows us to compare the performance for both methods at equivalent spatial discretization. In these conditions the execution time of the MIT-GEM is around 50\% longer. This is inherently due to the use of the RBF-FD method for the solution of the diffusion equation and due to the node regeneration required by the changing domain shape. These factors appear to prevail over those that are expected to decrease the computation time: smaller number of nodes, computationally cheaper front tracking by moving nodes instead of solving a phase-field equation, a smaller number of envelope velocity calculations.
}

\hl{
With refinement of the envelope node spacing, $\he$, we can observe a clear gain of the MIT-GEM over the PFIC-GEM. At the finest discretization, $\he=0.03$, the MIT-GEM is around three times faster. This gain is exclusively due to the \h-adaptivity of the spatial discretization in the MIT-GEM, which allows us to use significantly less nodes in the domain, compared to a uniform node distribution, while retaining a high node density on the envelope and thus a high accuracy of the solution in the regions of interest. For the problem at hand (the simulation of a single dendrite in a fairly large domain, constant node spacing at the outer boundary) the computational complexity of the MIT-GEM is shown in Figure
}
~\ref{fig:execution_times} 
\hl{
to scale as $\sim \he^{-2.7}$, where $\he$ is the node spacing on the envelope. For the PFIC-GEM it scales as $\sim \he^{-4}$, which is equivalent to $O(N^2)$, where $N$ is the number of mesh points. Note that the time step is $\sim \he^2$ for both methods. These results show that the MIT-GEM with \h-adaptivity is particularly interesting for high-accuracy solutions, using small $\he$, where \h-adaptivity is highly beneficial.
}

\begin{figure}
	\centering
	\includegraphics[width=0.7\textwidth]{figures/execution_times.eps}
	% \includegraphics[width=\textwidth]{figures/execTimes.png}
	\caption{Execution times for PFIC and MIT methods as function of the node spacing at the envelope, $\he$. Note that for the MIT the node spacing at the outer boundary was constant ($h_\ell=0.1$) and for the PFIC the meshes are uniform, with spacing $\he$. The times were measured on the same hardware setup assuring CPU affinity and fixed CPU frequency at 2.4GHz. The regression functions give an estimation of the computational complexity.}
	\label{fig:execution_times}
\end{figure}



\section{Simulation of interacting grains}

\hl{
	One of the primary interests of the GEM is to study solutal interactions between grains over distances that are of the order of the grain size. In this section we show two examples of application of the MIT-GEM.

	In Figure
}
~\ref{fig:concentration_double}
\hl{
	we show MIT-GEM and PFIC-GEM simulations of two dendrites as they grow close to each other. A drawback of the PFIC when describing grains that are relatively close ($\sim \delta$) is that the phase-fields of the individual grains can interact and can thus provide envelope shapes that are not consistent with the GEM model. Due to the properties of the phase-field equations, two envelopes that are described by the same phase field can merge if they are sufficiently close. This is shown in Figure
}
~\ref{fig:concentration_double},
\hl{
	where two dendrite envelopes simulated by the PFIC-GEM using a single phase field  actually merge. This result is not consistent with the GEM model and the merging is only a consequence of the PFIC method. A way of avoiding merging of envelopes in the PFIC-GEM is to use a multi-phase-field approach, i.e., to use a distinct phase field for each grain. This is a robust method, but it adds an additional equation for each phase field, which increases the computation time and becomes costly when simulating large numbers of grains (100 or more). The MIT-GEM entirely avoids such merging due to numerical artefacts, the interactions between the envelopes are strictly as described by the GEM, they are given by the concentration field between the grains. The growth of the two envelopes shown in Figure
}
~\ref{fig:concentration_double}
\hl{
	stops at a distance of around 1.1. Note that the distance is somewhat larger than $\delta=1$ because the concentration of the liquid between the grains reaches 0 before the grains actually grow down to the distance of $\delta$. For clarity, we plot the solute concentration along the $y=x$ and $y=-x$ lines in Figure
}
~\ref{fig:concentration_double}
\hl{
	on the right. We see that the solute concentration between the dendrites is $0$, which stops further growth of the envelope in the MIT approach, consistently with the GEM model.
}

% \mz{Figure 18: Popravil sem caption. Dodati je treba še envelope za PFIC-GEM z dvema PF poljema (predlagam z rumeno črto, kot nakazano v captionu).}
\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{figures/2_concentration_b.png}
	\caption{Two interacting dendrites at time $t=7.184$. Left: The MIT-GEM concentration field is shown. The envelopes predicted by PFIC-GEM using a single phase field are shown in top right by a green line, for two distinct phase fields with a red line while MIT envelopes are shown in blue. The grain seeds were positioned at $\b r_1 = (2, 2)$ and $\b r_2 = (-2, -2)$. The bottom right figure shows concentration profiles along the diagonals (shown in magenta in the left image).}
	\label{fig:concentration_double}
\end{figure}
%The simulation was started with two grains positioned at $\b r_1 = (2, 2)$ and $\b r_2 = (-2, -2)$ where the domain was discretized using $h_e\approx0.0775$ and $h_l=0.1$, and the time step was set to $\mathrm{d}t = 3.005\cdot 10^{-4}$.

\hl{
	To demonstrate a more general application of the MIT-GEM, we show a simulation of $6$ dendrites with different rotations of the four tip growth directions. The time evolution of the interacting grain shapes and of the solute concentration field is shown in Figure
}
~\ref{fig:time_evolution_6}.
\hl{
	Adding grains with different orientations in the MIT approach is straightforward. Each grain has its distinct set of envelope boundary nodes (evolving during the simulation) and can thus be easily attributed its own set of tip growth directions. This is an advantage with respect to the PFIC-GEM approach, where the multi-phase-field approach must be used to describe multiple grain orientations, introducing additional computational cost.
}

\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{figures/6_dendrites_time_evolution.png}
	\caption{Growth of six interacting dendrites with different orientations of the tip growth directions.}
	\label{fig:time_evolution_6}
\end{figure}