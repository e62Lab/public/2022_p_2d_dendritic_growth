\section{GEM with phase-field interface capturing on a fixed mesh (PFIC)}
\label{sec:diffuse}

The standard formulations of GEM~\cite{souhar2016three,steinbach2005transient,steinbach1999three} use a PFIC method~\cite{sun2007sharp} to track the grain envelope on a fixed mesh. In PFIC, the tracked front is given by the level set $\phi=\phi\env$ of a continuous indicator field $\phi$. The transition of $\phi$ between 1 (grain) and 0 (liquid) is smooth but compact; it follows a hyperbolic tangent profile with characteristic width $W_\phi$~\cite{sun2008atwo,souhar2016three}
\begin{equation}
    \label{eq:diffuse-kernel}
    \phi(n) = \frac{1}{2} \left[ 1 - \tanh \left( \frac{n}{2 W_\phi} \right) \right] \,,
\end{equation}
where $n$ is the distance from the center of the hyperbolic tangent profile. The evolution of $\phi$ is given by a phase-field equation that ensures the transition is self-preserving and retains its shape and width
\begin{equation}
    \label{eq:diffuse}
    \frac{\partial \phi}{\partial t}
    + v_n \b n \cdot \nabla \phi =
    -b
    \underbrace{
        \left[
            \lap \phi
            - \frac{\phi(1 - \phi)(1-2\phi)}{W_\phi^2}
            - \left| \nabla \phi \right| \nabla \cdot \left( \frac{\nabla \phi}{\left| \nabla \phi \right|} \right)
            \right]
    }_{\mathrm{stabilization \ term}},
\end{equation}
where $v_n$ is the envelope growth speed defined as a scalar field. The term on the right hand side of the equation is a stabilization term that ensures the phase-field retains the hyperbolic tangent transition. The coefficient $b$ is a numerical parameter that controls the relaxation of the phase-field profile. Generic criteria on how to choose $b$ and $W_\phi$ in order to minimize the error and to ensure the stability of the PFIC method are discussed by Sun and Beckermann~\cite{sun2007sharp}. Specific guidelines for the use of the PFIC for envelope tracking in the GEM are provided by Souhar et al.~\cite{souhar2016three}. In this work the parameters $\phi\env$, $W_\phi$ and $b$, as well as the grid size and time step are selected following the guidelines of Souhar et al.~\cite{souhar2016three}. Note that the choice of $\phi\env=0.95$ is to favour the accuracy of the tip of the envelope in a convex shape.

%FROM sun2008atwo:
%The term in the square brackets on the right-hand side of Equation (24) is added to the continuity equation, Equation (15), to sustain the hyperbolic tangent φ profile across the diffuse interface during motion. The last term in the square brackets is the so-called counter-term [4] that cancels out curvature-driven interface motion at leading order for a finite interface width [see also Equation (6)]. For a flat interface where κ = 0, the last term in the square brackets is equal to zero and the first two terms (i.e. ∇2
%φ−φ (1 − φ) (1 − 2φ) /δ2) yield the hyperbolic
%tangent φ profile as the stationary solution of Equation (24). During interface motion, the term in the square brackets sustains the hyperbolic tangent φ profile, while the ∇2
%φ term smoothes out
%interface singularities.

The main advantage of this computational method is that it avoids explicit tracking of the envelope and instead solves the PDE from Equation~\eqref{eq:diffuse} on a fixed mesh. Additionally, Sun and Beckermann~\cite{sun2007sharp} demonstrated its versatility and robustness as a general front capturing method. When coupled with the GEM, however, a few aspects of the method require further attention.
\begin{description}
    %
    \item[Accurate calculation of the front speed.]
        %In the GEM the front speed depends on the concentration at a given distance, $\delta$, from the front, $u_\delta$. The speed of point $\b x\env$ on the front is $v_n(\b x\env) = f(u_\delta, \delta)$, where the function $f$ is given by Equations~\eqref{eq:cantorvogel} and~\eqref{eq:tipspeed}. Note that $f$ is a strictly increasing function of $u_\delta$ for any given $\delta$. Also note that in the vicinity of the envelope the dimensionless concentration, $u$, strictly increases with distance from the envelope. This means that an error that overestimates the distance of the stagnant-film from the envelope, results in an overestimated speed of the envelope. An underestimation of the distance results in an underestimated speed. When the phase-field equation~\eqref{eq:diffuse} is used, the speed $v_n$ cannot be calculated only on the sharp envelope, but needs to be defined as a field in the whole domain (In practice it is enough to provide $v_n$ where $\nabla \phi$ is non-negligible, i.e., in the vicinity of the front). An accurate determination of the distances to the envelope and to the stagnant-film from any mesh point is therefore crucial to obtain an accurate speed field. In principle these distances can be determined with the phase field, $\phi$, because it can be used as a distance function. But this distance function becomes inaccurate if the hyperbolic-tangent shape of the $\phi$ profile distorts. Small distortions are inevitable with any numerical solution method. The main problem of such distortions, pointed out by Souhar et al.~\cite{souhar2016three}, is that the resulting error of the envelope speed calculation is self-reinforcing and that it cannot be reduced by adjusting the relaxation parameter, $b$. Souhar et al.~\cite{souhar2016three} developed a front reconstruction method for the determination of the distance to the envelope, which removes these errors. In this method the envelope is reconstructed by marker points densely distributed over the surface defined by the level set of $\phi=\phi\env$. The distance to the envelope can then be accurately determined by the distance to the closest marker point. This reconstruction needs to be done at each timestep and therefore increases the computation time considerably.
        %
%        Although in theory the front speed $v_n$ should be computed over the entire domain to propagate the phase field with Equation~\eqref{eq:diffuse}, it is in practice enough to provide $v_n$ where $\nabla \phi$ is non-negligible, i.e., in the vicinity of the front.
        To propagate the phase field with Equation~\eqref{eq:diffuse}, the front speed, $v_n$, needs to be provided everywhere where $\nabla \phi$ is non-negligible, i.e., in the vicinity of the front.
        %Because the front speed depends on the positions of the front and of the stagnant-film, accurate determination of the distances to the envelope and to the stagnant-film from any mesh point $\x \in \Omega$ is crucial to obtain an accurate speed field.
        In the PFIC-based GEM the speed $v_n$ at point $\b x\in \Omega$ is equal to the speed of the closest point lying on the sharp front, i.e., $v_n (\b x) = v_n (\b x\env)$.
        %Approximations:
        %(1) CLOSEST MARKER POINT = CLOSEST POINT
        %(2) NORMALIZED GRADIENT IN x = NORMAL AT THE FRONT
        %(3) INTERPOLATION of u in x\delta by a second-order method
        Note that by definition the vector between a point and the closest point on the front, $\b x - \b x\env$, is normal to the front in $\b x\env$.
        The speed of point $\b x\env$ depends on the concentration at a given distance $\delta$ from the front, $u_\delta =u(\x_\delta)=u(\b x\env + \delta \, \b n)$, where $\b n$ is the normal vector to the front at point $\b x\env$ and $\x_\delta$ is the stagnant-film position. Thus, the speed of point $\b x\env$ is given by Equations~\eqref{eq:envelopespeed}, \eqref{eq:cantorvogel}, and~\eqref{eq:tipspeed} as a function of $u_\delta$, $\delta$, and $\b n$. Because the speed in $\b x$ is equal to the speed in $\b x\env$, we can write a functional expression $v_n(\b x) = f(\b x - \b x\env, u(\b x\env + \delta \, \b n), \delta)$, which shows the three quantities that need to be approximated in order to compute the frontal speed:
        \begin{enumerate}[(i)]
            \item the distance $\b x - \b x\env$ from the front,
            \item the vector $\b n$ normal to the front in $\b x\env$ and
            \item the interpolation of the concentration $u_\delta$ to the corresponding point on the stagnant-film $\b x_\delta =\b x\env + \delta \, \b n$.
        \end{enumerate}

        The accuracy of these three approximations determines the accuracy of the calculated speed, $v_n$.
        % It is important to note that $f$ is a strictly increasing function of $u_\delta$ for any given $\delta$. Also note that in the vicinity of the envelope the dimensionless concentration, $u$, strictly increases with distance from the envelope. This means that an error that overestimates the distance of the stagnant-film from the envelope, results in an overestimated speed of the envelope, and vice versa.
        In this work the PFIC-based GEM uses the front reconstruction method proposed by Souhar et al.~\cite{souhar2016three} to determine the distance $\b x - \b x\env$ from the front and the normal vector, $\b n$. In this method, the envelope is reconstructed by marker points densely distributed over the surface defined by the level set $\phi=\phi\env$. The distance from a point $\b x$ to the envelope is then approximated by the projection of the distance to the closest marker, $\b x_\text{m}$, on an approximated normal vector $$|\b x - \b x\env| \approx - (\b x - \b x_\mathrm{m}) \cdot \b n,$$ where the normal is approximated by a normalized gradient of the phase field in $\b x$ with the following expression
        \begin{equation}
            \label{eq:grad_normal}
            \b n \approx \frac{\nabla \phi(\b x)}{ \lvert \nabla \phi(\b x) \rvert}.
        \end{equation}
        The same normal vector approximation is used to calculate the stagnant-film point, $\b x_\delta$. The interpolation of $u_\delta$ in $\b x_\delta$ is further done by a second-order interpolation scheme.

        Note that, in principle, the distances can be determined via the phase field, $\phi$. However, as pointed out by Souhar et al.~\cite{souhar2016three}, such approach results in a self-reinforcing error and is not sufficiently accurate for practical use.
        %The front reconstruction method provides much better accuracy. Its disadvantage is that it increases the computation time considerably because the front reconstruction with markers needs to be done at each timestep.
        %
    \item[Damping of protrusions.]
        The stabilization term of Equation~\eqref{eq:diffuse} smoothens interface singularities due to the dissipative nature of the $\nabla^2 \phi$ term~\cite{sun2007sharp}. The damping of ripples on the envelope is controlled by the relaxation coefficient $b$. Unfortunately, it is not known to what extent such damping affects the development of envelope protrusions that have a physical meaning.
        %
    \item[Resolution of small curvature radii.]
        The phase-field method can accurately describe convex radii of front curvature larger than $W_\phi \left[ 6 - \ln \left( \frac{\phi\env}{1-\phi\env} \right) \right]$ and concave radii larger than $W_\phi \left[ 6 + \ln \left( \frac{\phi\env}{1-\phi\env} \right) \right]$~\cite{souhar2016three}. With standard mesh spacing of $h\leq W_\phi / \sqrt{2}$~\cite{souhar2016three,sun2007sharp}, this means that the resolution of shape representation is directly proportional to the fixed mesh spacing, implying that small-scale front features like protrusions may not be adequately resolved on fixed meshes.
        %Also, the radius of the initial envelopes must be sufficiently large if one wants to exclude any errors linked to the front cacpturing method. 
        %
\end{description}

%If these aspects are not handled carefully, they can cause undesired numerical artifacts. Such artifacts are possibly detrimental in describing highly non-linear phenomena, such as the formation of new branches of the dendrite envelope or the development of branches during the initial stages of dendrite growth. It has been shown that the representation of such branching by the GEM is faithful to the physics~\cite{viardin2017mesoscopic,viardin2020mesoscopic}, but the role of numerical artifacts linked to the envelope tracking method has, to the best of our knowledge, not yet been verified.

%To perform such verification, an alternative, numerically accurate implementation of the GEM is required. To provide high accuracy, the numerical methods employed should minimize the three types of numerical artifacts presented above. The sharp-interface tracking method combined with an adaptive spatial discretization, developed in this work provides such method. Because it is an interface tracking method, it avoids the complexity of having to provide a speed field over the entire domain. Instead, the speed is calculated only on the front. The methods for the calculation of the normal and for the interpolation of the stagnant-film concentration are carefully assessed in order to ensure high accuracy computation of the front speed. Interface tracking also avoids the damping of protrusions. Finally, the adaptive spatial discretization provides capability for describing small radii of curvature and also improves accuracy of the concentration field in the vicinity of the front. Because of these characteristics, further discussed in this paper, the meshless methods can be used as a reference for the verification and calibration of the PFIC-based GEM.

% \mz{Gregor Kosec:\\
%     v section 3 si dodal super diskusijo o tem na kaj moramo biti pozorni pfi FVM resevanju. Ne vem pa ce znam (oz sem prelen) enolicno dolocit probleme.

%     Kako ti racunas normalo? \\
%     \emph{OK, odgovorjeno v tekstu.}

%     Kako racunas velocity -- zares me zanima kako zracunas conc field v smeri normale?\\
%     \emph{OK, odgovorjeno v tekstu.}

%     Koliko je debelina envelopeja zaradi phasi field interfaceja?\\
%     $W_\phi = \sqrt(2) h$~\cite{souhar2016three}

%     Ali to vpliva na natančnosti izračuna hitrosti?\\
%     \emph{Kar zadeva debelino envelopea, ne neposredno. Lahko vpliva na natančnost izračuna normale, ampak pri uporabljenih debelinah je to po mojem zanemarljivo. Kar se tiče ostalih aproksimacij, je to zdaj razloženo v tekstu. Če povzamem, moramo natančno izračunati 3 stvari: razdaljo do fronte, normalo in interpolacijo koncentracije na stagnant filmu. Izračun razdalje pri interface trackingu odpade. Izračun normale in interpolacije pa smo v tem članku podrobno analizirali.}

%     Lahko zgornje tocke povezeva z:

%     Accurate calculation of the front speed?\\
%     \emph{To lahko povezemo z natančnostjo izračuna razdalje do fronte in normale na fronto. Pri tem so uporabljene določene aproksimacije in to lahko vpliva na natančnost. Glej nov tekst zgoraj.}

%     Damping of protrusions.\\
%     \emph{To je povezano z PFIC metodo, torej z debelino interfacea, torej ja. Interface tracking metoda se temu problemu izogne.}

%     Resolution of small radii of curvature. -- to je pa stvar gostote mreze.\\
%     \emph{...in debeline interfacea, ki pa je odvisna od gostote mreže. Torej je to spet povezano tako z PFIC metodo, kot z lokalno gostoto mreže ob fronti. }
% }
% \mz{Vprašanje: ali poznamo red konvergence interpolacije koncentracije?\\
%     MJ: Seveda.}
