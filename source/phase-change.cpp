#include <medusa/Medusa_fwd.hpp>

#include "dendrite/Dendrite.hpp"
#include "helpers/helper.hpp"

using namespace mm;
using namespace std;

#define EPS 1e-8



void StartSimulation(const XML& conf, HDF& hdf) {
    const bool debug = conf.get<bool>("debug.print");
    // Get dendrite domain properties.
    auto dendrite_initial_size = conf.get<double>("domain.dendrite.initial_size");
    auto dendrite_type = conf.get<int>("domain.dendrite.type");
    auto dendrite_h = conf.get<double>("domain.dendrite.h");
    // Get liquid domain properties.
    auto liquid_size = conf.get<double>("domain.liquid.size");
    auto liquid_type = conf.get<int>("domain.liquid.type");
    auto liquid_h = conf.get<double>("domain.liquid.h");
    auto dendrite_xs = get_vectors(conf, "x");
    auto dendrite_ys = get_vectors(conf, "y");
    auto dendrite_fis = get_vectors(conf, "fi");
    int number_of_dendrites = dendrite_xs.size();
    prn(dendrite_xs)
    prn(dendrite_ys)

    // Fill.
    auto fill_samples = conf.get<int>("domain.fill_samples");
    int seed = conf.get<int>("domain.fill_seed");
    if (seed == -1) {
        seed = get_seed();
    }

    // **********************
    // Create initial domain.
    // **********************
    if (debug) {
        cout << "Creating initial domain ..." << endl;
    }
    // Liquid domain.
    BoxShape<Vec2d> box_liquid(-liquid_size, liquid_size);
    DomainDiscretization<Vec2d> domain =
        box_liquid.discretizeBoundaryWithStep(liquid_h, liquid_type);
    if (conf.get<bool>("debug.anisotropy")) {
        BallShape<Vec2d> shape(0, liquid_size);
        DomainDiscretization<Vec2d> domain =
            shape.discretizeBoundaryWithStep(liquid_h, liquid_type);
    }
    KDTree<Vec2d> test_tree(domain.positions());
    for (int i : domain.boundary()) {
        domain.type(i) = conf.get<int>("domain.liquid.type");
    }

    // Initial dendrite boundary shape.
    for (int i = 0; i < dendrite_xs.size(); ++i) {
        BallShape<Vec2d> ball_dendrite({dendrite_xs[i], dendrite_ys[i]}, dendrite_initial_size);
        DomainDiscretization<Vec2d> _initial_dendrite_domain =
            ball_dendrite.discretizeBoundaryWithStep(dendrite_h, dendrite_type - i);

        // Difference.
        domain -= _initial_dendrite_domain;
    }

    // Remove corner nodes.
    auto borders = domain.shape().bbox();
    Range<int> corner = domain.positions().filter([&](const Vec2d& p) {
        // remove nodes that are EPS close to more than 1 border
        return (((p - borders.first) < EPS).size() + ((borders.second - p) < EPS).size()) > 1;
    });
    domain.removeNodes(corner);

    // Fill engine.
    GeneralFill<Vec2d> fill;
    fill.numSamples(fill_samples).seed(seed);
    // KD-tree with boundary nodes.
    KDTree<Vec2d> tree(domain.positions()[domain.types() <= dendrite_type]);
    auto fn = [&](const Vec2d& p) { return dx(p, tree, test_tree, conf); };
    // Fill domain with nodes.
    domain.fill(fill, fn);

    // Run simulation.
    Dendrite<Vec2d>::Solve(conf, hdf, domain);
}

int main(int argc, char* argv[]) {
    // Check for settings file.
    assert_msg(argc >= 2, "Second argument should be the XML parameter file.");

    // Timer initialization.
    Timer t;
    t.addCheckPoint("start");

    // Read input config.
    cout << "Reading params from: " << argv[1] << endl;
    const XML conf(argv[1]);

    // Set num threads.
#if defined(_OPENMP)
    omp_set_num_threads(conf.get<int>("sys.threads"));
#endif

    // Create H5 file to store output.
    string output_file =
        conf.get<string>("meta.directory_out") + conf.get<string>("meta.filename_out") + ".h5";
    cout << "Creating results file: " << output_file << endl;
    HDF hdf(output_file, HDF::DESTROY);

    // Write params to results file.
    hdf.writeXML("conf", conf);
    hdf.close();

    // Run solution procedure.
    if (conf.get<int>("debug.print") == 1) {
        cout << "Running solution procedure .." << endl;
    }
    StartSimulation(conf, hdf);

    // End execution.
    t.addCheckPoint("end");
    prn(t.duration("start", "end"));
    cout << "Calculations saved to: " << output_file << "." << endl;

    // Save timer.
    hdf.atomic().writeTimer("timer", t);

    return 0;
}
