\section{Mesoscopic Grain Envelope Model (GEM) for equiaxed isothermal solidification}
\label{sec:model}

The GEM represents a dendritic grain by its envelope. The dendrite envelope is an artificial smooth surface that connects the tips of the actively growing dendrite branches. The idea behind this simplified representation of a dendrite is that the solute interactions between the grains can be accurately simulated without the need for a detailed representation of the branched structure of the solid-liquid interface. Because of the simplified shape, the computational cost of the GEM is several orders of magnitude smaller than that of models that represent the branched structure of the dendrite in detail~\cite{tourret2020comparing,viardin2020mesoscopic}.

Figure~\ref{fig:gem} illustrates the key concepts of the GEM. The grain is delimited by the dendrite envelope and its growth is controlled by solute diffusion from the envelope into the adjacent, fully liquid domain. The grain contains both liquid and solid phases. The liquid within the grain and on the envelope is assumed to be in a state of thermodynamic equilibrium. In the case of a binary alloy, this means that the solute concentration is determined by the temperature. In the isothermal system considered in this paper, the temperature is uniform and constant throughout the entire domain (i.e., over several dendrites), resulting in a uniform and constant solute concentration on the envelope. It should be noted that the dimensionless model formulation presented below is only applicable to isothermal solidification, as it assumes a uniform and constant solute concentration on the envelope. More general model descriptions can be found in the literature~\cite{delaleau2010mesoscopic,viardin2020mesoscopic}.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.6\textwidth]{figures/gem_sketch.jpg}
    \caption{Schematic illustration of the main concepts of the GEM.}
    \label{fig:gem}
\end{figure}

The GEM assumes that dendrite branches grow in predefined growth directions\footnote{For example, in $\langle 1 0 0 \rangle$ crystal directions for a typical cubic crystal.}. The growth speed $v_n$ of the dendrite envelope is then given by the speed of the tips $v$ of the dendrite branches by the relation
\begin{equation}
    \label{eq:envelopespeed}
    v_n = v \cos(\vartheta),
\end{equation}
where $\vartheta$ is the smallest of the angles between the outward normal $\b n$ to the envelope and the individual growth directions of the tips. In the present study, the branches are given $\left\langle 10 \right\rangle$ directions, i.e., four possible directions within a two-dimensional domain. The tip speed is calculated from a stagnant-film formulation of the 2D Ivantsov solution~\cite{cantor1977dendritic} which relates the Péclet number $\text{Pe}$ of the tip to the normalized dimensionless concentration $u_\delta$ of the liquid at a distance $\delta$ from the tip, as
\begin{equation}
    \label{eq:cantorvogel}
    u_\delta = \sqrt{\pi \text{Pe}}  \exp(Pe)  \left[ \operatorname{erfc} \left( \sqrt{\text{Pe}} \right) - \operatorname{erfc} \left( \sqrt{\text{Pe}+\delta} \right) \right]
\end{equation}
and from the so-called tip selection criterion that in dimensionless form reads
\begin{equation}
    \label{eq:tipspeed}
    v = (\text{Pe}/\text{Pe}_\text{Iv})^2,
\end{equation}
where $\text{Pe}_\text{Iv}$ is a constant\footnote{$\text{Pe}_\text{Iv}$ is the Péclet number of a free dendrite tip, i.e., a tip growing into an infinite liquid with far-field concentration $u_0$, given by the solution of $u_0 = \sqrt{\pi \text{Pe}_\text{Iv}} \exp(\text{Pe}_\text{Iv}) \operatorname{erfc} \left( \sqrt{\text{Pe}_\text{Iv}} \right)$. Here, solute concentration $u_0$ is a physical parameter of the model.}. Equations~\eqref{eq:cantorvogel} and~\eqref{eq:tipspeed} are used to solve for $v$ at any given point on the envelope. The concentration $u_\delta$ is given by the concentration field in the liquid around the envelope, which is resolved numerically from the diffusion equation
\begin{equation}
    \label{eq:diffusion}
    \frac{ \partial u}{\partial t} = \lap u .
\end{equation}
Equations~(\ref{eq:envelopespeed}--\ref{eq:diffusion}) define the GEM, where the stagnant-film thickness, $\delta$, is a model parameter and $\text{Pe}_\text{Iv}$ is a physical parameter, function of the far-field concentration, $u_0$. The boundary condition for Equation~\eqref{eq:diffusion} on the envelope is $u = 0$. The model also considers that the liquid concentration inside the envelope (grain) is $u=0$.
\hl{
    This means that where the stagnant film overlaps with a neighbouring grain, the stagnant-film concentration, $u_\delta$, becomes $0$ and then the velocity given by Equation
}
~\eqref{eq:tipspeed}
\hl{
    is also zero. The envelope growth therefore stops when two dendrite envelopes grow near to a distance equal to the stagnant-film thickness, $\delta$. In order for the envelope velocity model to provide a physically correct growth velocity, $\delta$ needs to be of a certain thickness and should not be significantly smaller or larger (see refs.
}
~\cite{souhar2016three}, \cite{tourret2020comparing}).
\hl{
    In the present work the dimensionless thickness of the stagnant film is $\delta=1$.
}

In the simulations presented in this paper the solute diffusion flux across the outer domain boundary is zero, i.e., the boundary condition for Equation~\eqref{eq:diffusion} on the outer liquid boundary is $\b n \cdot \nabla u = 0$, where $\b n$ is the normal vector to the outer boundary $\Gamma_\ell$. The initial concentration of the liquid is $u = u_0$, i.e., the liquid is homogeneous and has a dimensionless supersaturation of $u_0$. In this work we use a stagnant-film thickness of $\delta = 1$.

Note that all equations are written in terms of dimensionless quantities, where the dimensionless concentration is defined by $$u=\frac{1-\frac{C}{C^\star}}{1-k},$$ with the dimensional concentration $C$ , the equilibrium concentration $C^*$ of the liquid at the given solidification temperature and the equilibrium solid-liquid solute partition coefficient $k$. The other characteristic scales for normalization are $v_\text{Iv}$ for velocity, $D/v_\text{Iv}$ for length, $D / v_\text{Iv}^2$ for time, where $v_\text{Iv}=4\sigma^* D \text{Pe}_\text{Iv}^2/d_0$ is the steady-state velocity of the free tip, $\sigma^*$ is the tip selection parameter, $D$ is the diffusion coefficient, and $d_0$ is the capillary length.
