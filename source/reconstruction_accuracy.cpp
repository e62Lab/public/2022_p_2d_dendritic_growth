#include <medusa/Medusa.hpp>
#include "helpers/SplineShape.h"

using namespace mm;
using namespace std;

void evaluateRecon(HDF& hdf, const DomainDiscretization<Vec2d> domain,
                   const std::function<Vec2d(Vec<double, 1>)> example_r,
                   const std::function<Vec2d(Vec<double, 1>)> der_example_r,
                   const int spline_degree, const string spacing) {
    Eigen::VectorXd angles(domain.size());
    Eigen::VectorXd err_pos(domain.size());
    Eigen::VectorXd err_normal_angle(domain.size());
    omp_set_num_threads(10);
#pragma omp parallel for default(shared)
    for (int i = 0; i < domain.size(); ++i) {
        auto pos = domain.pos(i);
        double angle = std::atan2(pos(1), pos(0));
        //        angle = (angle < 0) ? 2 * mm::PI + angle : angle; // Map to 0->2PI
        angles[i] = angle;

        // ERROR by position.
        auto pos_analytic = example_r(Vec1d{angle});
        auto diff = (pos - pos_analytic).norm();
        err_pos[i] = diff;

        // ERROR by normal vector angle.
        auto normal = domain.normal(i);
        double normal_angle = std::atan2(normal(1), normal(0));
        //        normal_angle = (normal_angle < 0) ? 2 * mm::PI + normal_angle : normal_angle;//
        //        Map to 0->2PI
        normal_angle *= 180 / mm::PI;

        auto analytic_tangent = der_example_r(Vec1d{angle});
        analytic_tangent.normalize();
        auto analytic_normal = Vec2d{analytic_tangent(1), -analytic_tangent(0)};

        double analytic_normal_angle = std::atan2(analytic_normal(1), analytic_normal(0));
        //        analytic_normal_angle = (analytic_normal_angle < 0) ? 2 * mm::PI +
        //        analytic_normal_angle
        //                                                            : analytic_normal_angle;  //
        //                                                            Map to 0->2PI
        analytic_normal_angle *= 180 / mm::PI;

        double angle_diff = abs(normal_angle) - abs(analytic_normal_angle);
        err_normal_angle[i] = angle_diff;

        if (abs(angle_diff) > 100) {
            prn(pos) prn(pos_analytic) prn(normal) prn(analytic_normal) prn(diff) prn(normal_angle)
                prn(analytic_normal_angle) prn(angle_diff) prn("--")
        }
    }

    hdf.atomic().writeEigen(mm::format("domain_spline_%d_%s/error_pos", spline_degree, spacing),
                            err_pos);
    hdf.atomic().writeEigen(mm::format("domain_spline_%d_%s/angle", spline_degree, spacing),
                            angles);
    hdf.atomic().writeEigen(mm::format("domain_spline_%d_%s/error_angle", spline_degree, spacing),
                            err_normal_angle);
}

void revert_and_normalize(DomainDiscretization<Vec2d>& domain) {
    for (int i = 0; i < domain.size(); ++i) {
        auto normal = domain.normal(i);
        normal.normalize();
        domain.normal(i) = -normal;
    }
}

int main(int argc, char* argv[]) {
    // Check for settings file.
    assert_msg(argc >= 2, "Second argument should be the XML parameter file.");

    // Read input config.
    cout << "Reading params from: " << argv[1] << endl;
    const XML conf(argv[1]);

    // Create H5 file to store output.
    string output_file = conf.get<string>("meta.directory_out") +
                         (conf.get<bool>("domain.circle") ? "circle" : "flower") + "_" +
                         conf.get<string>("meta.filename_out") + ".h5";
    cout << "Creating results file: " << output_file << endl;
    HDF hdf(output_file, HDF::DESTROY);

    // Write params to results file.
    hdf.writeXML("conf", conf);
    hdf.close();

    // *******
    // Domain.
    // *******
    std::function<Vec2d(Vec<double, 1>)> example_r;
    std::function<Vec2d(Vec<double, 1>)> der_example_r;

    if (conf.get<bool>("domain.circle")) {
        example_r = [](Vec<double, 1> t) {
            double r = 1;

            return Vec2d(r * cos(t(0)), r * sin(t(0)));
        };

        der_example_r = [](Vec<double, 1> t) {
            double r = 1;
            Eigen::Matrix<double, 2, 1> jm;
            jm.col(0) << -r * sin(t(0)), r * cos(t(0));

            return jm;
        };
    } else {
        example_r = [](Vec<double, 1> t) {
            double r = pow(abs(cos(1.5 * t(0))), sin(3 * t(0)));
            return Vec2d(r * cos(t(0)), r * sin(t(0)));
        };

        der_example_r = [](Vec<double, 1> t) {
            double r = pow(abs(cos(1.5 * t(0))), sin(3 * t(0)));
            double der_r =
                (-1.5 * pow(abs(cos(1.5 * t(0))), sin(3 * t(0))) * sin(3 * t(0)) * sin(1.5 * t(0)) +
                 3 * pow(abs(cos(1.5 * t(0))), sin(3 * t(0))) * cos(3 * t(0)) * cos(1.5 * t(0)) *
                     log(abs(cos(1.5 * t(0))))) /
                cos(1.5 * t(0));

            Eigen::Matrix<double, 2, 1> jm;
            jm.col(0) << der_r * cos(t(0)) - r * sin(t(0)), der_r * sin(t(0)) + r * cos(t(0));

            return jm;
        };
    }

    // Define parametric curve's domain.
    BoxShape<Vec1d> param_bs(Vec<double, 1>{0.0}, Vec<double, 1>{2 * mm::PI});

    // Enforce first point at (1,0);
    //    domain.addBoundaryNode(example_r(Vec1d{0}), -1, der_example_r(Vec1d{0}));
    //    domain_orig.addBoundaryNode(example_r(Vec1d{0}), -1, der_example_r(Vec1d{0}));

    auto spacings = split(conf.get<string>("domain.h"), ',');
    for (string spacing : spacings) {
        UnknownShape<Vec2d> shape;
        DomainDiscretization<Vec2d> domain(shape);       // Acutal domain for fitting.
        DomainDiscretization<Vec2d> domain_orig(shape);  // Domain for analysis.

        prn(spacing);
        double d = stod(spacing);
        auto nodal_spacing = [=](Vec2d p) { return d; };

        GeneralSurfaceFill<Vec2d, Vec1d> gsf;
        gsf.seed(conf.get<int>("domain.seed"));
        domain.fill(gsf, param_bs, example_r, der_example_r, nodal_spacing);
        hdf.atomic().writeDomain(mm::format("domain_%s", spacing), domain);
        prn(domain.size());

        auto positions = domain.positions();
        if (conf.get<bool>("shuffle.do")) {
            auto rng = std::default_random_engine{};
            std::shuffle(std::begin(positions), std::end(positions), rng);
        }

        // Reconstruction with spline degree = 1.
        {
            SplineShape<Vec2d, 1> spline_shape(positions, conf.get<int>("domain.seed"));
            spline_shape.setInterior({0.1, 0.1}, true);
            DomainDiscretization<Vec2d> spline_domain =
                spline_shape.discretizeBoundaryWithStep(conf.get<double>("domain.h_refine"), -5);
            revert_and_normalize(spline_domain);
            // Save refined domain.
            hdf.atomic().writeDomain(mm::format("domain_spline_%d_%s", 1, spacing), spline_domain);
            // Evaulate reconstruction accuracy.
            evaluateRecon(hdf, spline_domain, example_r, der_example_r, 1, spacing);
        }
        // Reconstruction with spline degree = 2.
        {
            SplineShape<Vec2d, 2> spline_shape(positions, conf.get<int>("domain.seed"));
            spline_shape.setInterior({0.1, 0.1}, true);
            DomainDiscretization<Vec2d> spline_domain =
                spline_shape.discretizeBoundaryWithStep(conf.get<double>("domain.h_refine"), -5);
            revert_and_normalize(spline_domain);
            // Save refined domain.
            hdf.atomic().writeDomain(mm::format("domain_spline_%d_%s", 2, spacing), spline_domain);
            // Evaulate reconstruction accuracy.
            evaluateRecon(hdf, spline_domain, example_r, der_example_r, 2, spacing);
        }
        // Reconstruction with spline degree = 3.
        {
            SplineShape<Vec2d, 3> spline_shape(positions, conf.get<int>("domain.seed"));
            spline_shape.setInterior({0.1, 0.1}, true);
            DomainDiscretization<Vec2d> spline_domain =
                spline_shape.discretizeBoundaryWithStep(conf.get<double>("domain.h_refine"), -5);
            revert_and_normalize(spline_domain);
            // Save refined domain.
            hdf.atomic().writeDomain(mm::format("domain_spline_%d_%s", 3, spacing), spline_domain);
            // Evaulate reconstruction accuracy.
            evaluateRecon(hdf, spline_domain, example_r, der_example_r, 3, spacing);
        }
        // Reconstruction with spline degree = 4.
        {
            SplineShape<Vec2d, 4> spline_shape(positions, conf.get<int>("domain.seed"));
            spline_shape.setInterior({0.1, 0.1}, true);
            DomainDiscretization<Vec2d> spline_domain =
                spline_shape.discretizeBoundaryWithStep(conf.get<double>("domain.h_refine"), -5);
            revert_and_normalize(spline_domain);
            // Save refined domain.
            hdf.atomic().writeDomain(mm::format("domain_spline_%d_%s", 4, spacing), spline_domain);
            // Evaulate reconstruction accuracy.
            evaluateRecon(hdf, spline_domain, example_r, der_example_r, 4, spacing);
        }
        // Reconstruction with spline degree = 5.
        {
            SplineShape<Vec2d, 5> spline_shape(positions, conf.get<int>("domain.seed"));
            spline_shape.setInterior({0.1, 0.1}, true);
            DomainDiscretization<Vec2d> spline_domain =
                spline_shape.discretizeBoundaryWithStep(conf.get<double>("domain.h_refine"), -5);
            revert_and_normalize(spline_domain);
            // Save refined domain.
            hdf.atomic().writeDomain(mm::format("domain_spline_%d_%s", 5, spacing), spline_domain);
            // Evaulate reconstruction accuracy.
            evaluateRecon(hdf, spline_domain, example_r, der_example_r, 5, spacing);
        }
        // Reconstruction with spline degree = 6.
        {
            SplineShape<Vec2d, 6> spline_shape(positions, conf.get<int>("domain.seed"));
            spline_shape.setInterior({0.1, 0.1}, true);
            DomainDiscretization<Vec2d> spline_domain =
                spline_shape.discretizeBoundaryWithStep(conf.get<double>("domain.h_refine"), -5);
            revert_and_normalize(spline_domain);
            // Save refined domain.
            hdf.atomic().writeDomain(mm::format("domain_spline_%d_%s", 6, spacing), spline_domain);
            // Evaulate reconstruction accuracy.
            evaluateRecon(hdf, spline_domain, example_r, der_example_r, 6, spacing);
        }
        //        // Reconstruction with spline degree = 10.
        //        {
        //            SplineShape<Vec2d, 10> spline_shape(domain.positions(),
        //            conf.get<int>("domain.seed")); spline_shape.setInterior({0.1, 0.1}, true);
        //            DomainDiscretization<Vec2d> spline_domain =
        //                spline_shape.discretizeBoundaryWithStep(conf.get<double>("domain.h_refine"),
        //                -5);
        //            revert_and_normalize(spline_domain);
        //            // Save refined domain.
        //            hdf.atomic().writeDomain(mm::format("domain_spline_%d_%s", 10, spacing),
        //            spline_domain);
        //            // Evaulate reconstruction accuracy.
        //            evaluateRecon(hdf, spline_domain, example_r, der_example_r, 10, spacing);
        //        }
    }

    // Complete.
    return 0;
}
