% \documentclass[preprint,12pt]{elsarticle}
%% Use the option review to obtain double line spacing
%% \documentclass[authoryear,preprint,review,12pt]{elsarticle}

%% Use the options 1p,twocolumn; 3p; 3p,twocolumn; 5p; or 5p,twocolumn
%% for a journal layout:
% \documentclass[final,1p,times]{elsarticle}
%% \documentclass[final,1p,times,twocolumn]{elsarticle}
\documentclass[final,3p,times]{elsarticle}
%% \documentclass[final,3p,times,twocolumn]{elsarticle}
% \documentclass[final,5p,times]{elsarticle}
%% \documentclass[final,5p,times,twocolumn]{elsarticle}

%% For including figures, graphicx.sty has been loaded in
%% elsarticle.cls. If you prefer to use the old commands
%% please give \usepackage{epsfig}

%% The amssymb package provides various useful mathematical symbols
\usepackage{amssymb}
%% The amsthm package provides extended theorem environments
\usepackage{amsthm}
%% The lineno packages adds line numbers. Start line numbering with
%% \begin{linenumbers}, end it with \end{linenumbers}. Or switch it on
%% for the whole article with \linenumbers.
\usepackage{lineno}
\usepackage{wrapfig}
\usepackage{soul}
\usepackage{footmisc}

% Custom.
%\usepackage[nocompress]{cite}
\usepackage{multirow}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{url}
\usepackage{todonotes}
\usepackage{units}
\usepackage{physics}
\usepackage{algpseudocode}  % za psevdokodo
\usepackage{algorithm}      % za
\usepackage{cancel}
\usepackage{caption}
\usepackage{booktabs}
\usepackage{varwidth}
\usepackage{subcaption}
\usepackage{xcolor,colortbl}
\definecolor{Gray}{gray}{0.9}
% Table float box with bottom caption, box width adjusted to content
\algnewcommand\algorithmicto{\textbf{to}}
\algnewcommand\algorithmicin{\textbf{in}}
\algnewcommand\algorithmicforeach{\textbf{for each}}
\algrenewtext{For}[3]{\algorithmicfor\ #1 $\gets$ #2\ \algorithmicto\ #3\ \algorithmicdo}
\algdef{S}[FOR]{ForEach}[2]{\algorithmicforeach\ #1\ \algorithmicin\ #2\ \algorithmicdo}

\newcommand\gk[1]{\todo[inline,color=cyan]{#1 --GK--}}
\newcommand\mj[1]{\todo[inline,color=pink]{#1 --MJ--}}
\newcommand\mr[1]{\todo[inline,color=yellow]{#1 --MR--}}
\newcommand\mz[1]{\todo[inline,color=orange]{#1 --MZ--}}

% Commands
\newcommand{\RR}{\textsuperscript{\textregistered} \hspace{1mm}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\T}{\mathsf{T}}
\renewcommand{\L}{\mathcal{L}}
\renewcommand{\b}{\boldsymbol}
\newcommand{\n}{\b{n}}
\newcommand{\x}{\b{x}}
\newcommand{\w}{\b{w}}
\newcommand{\eps}{\varepsilon}
\newcommand{\lap}{\nabla^2}
\newcommand{\dpar}[2]{\frac{\partial #1}{\partial #2}}
\newcommand{\einf}{e_\infty}
\newcommand{\Sph}[1]{B_{#1}}
\newcommand{\h}{\emph{h}}
\newcommand{\Iv}{_\mathrm{Iv}}
\newcommand{\env}{_\mathrm{env}}
\newcommand{\ienv}{_{i,\mathrm{env}}}
\newcommand{\he}{h_\mathrm{e}}
\newcommand*\rot{\rotatebox{90}}

\journal{Journal of Computational Physics}

\begin{document}

\begin{frontmatter}

    %% Title, authors and addresses

    %% use the tnoteref command within \title for footnotes;
    %% use the tnotetext command for theassociated footnote;
    %% use the fnref command within \author or \address for footnotes;
    %% use the fntext command for theassociated footnote;
    %% use the corref command within \author for corresponding author footnotes;
    %% use the cortext command for theassociated footnote;
    %% use the ead command for the email address,
    %% and the form \ead[url] for the home page:
%    \title{Sharp-interface dendritic envelope growth simulation}
    \title{Meshless interface tracking for the simulation of dendrite envelope growth}

    \author[ijs]{Mitja Jančič\corref{cor1}}
    \ead{mitja.jancic@ijs.si}

    \author[lor,damas]{Miha Založnik}
    \ead{miha.zaloznik@univ-lorraine.fr}
    \author[ijs]{Gregor Kosec}
    \ead{gregor.kosec@ijs.si}


    \address[ijs]{Institute Jožef Stefan, Parallel and Distributed Systems Laboratory, Jamova cesta 39, 1000 Ljubljana, Slovenia}
    % \address[mps]{International Postgraduate School Jožef Stefan, Jamova Cesta 39, 1000 Ljubljana, Slovenia}
    \address[lor]{Universit\'{e} de Lorraine, CNRS, IJL, Nancy, F-54000, France}
    \address[damas]{Laboratory of Excellence DAMAS, Universit\'{e} de Lorraine, Nancy/Metz, France}

    \cortext[cor1]{Corresponding author}

    \begin{abstract}
        The growth of dendritic grains during solidification is often modelled using the Grain Envelope Model (GEM), in which the envelope of the dendrite is an interface tracked by the Phase Field Interface Capturing (PFIC) method. In the PFIC method, an phase-field equation is solved on a fixed mesh to track the position of the envelope. While being versatile and robust, PFIC introduces certain numerical artefacts. In this work, we present an alternative approach for the solution of the GEM that employs a Meshless (sharp) Interface Tracking (MIT) formulation, which uses direct, artefact-free interface tracking. In the MIT, the envelope (interface) is defined as a moving domain boundary and the interface-tracking nodes are boundary nodes for the diffusion problem solved in the domain. To increase the accuracy of the method for the diffusion-controlled moving-boundary problem, an \h-adaptive spatial discretization is used, thus, the node spacing is refined in the vicinity of the envelope. MIT combines a parametric surface reconstruction, a mesh-free discretization of the parametric surfaces and the space enclosed by them, and a high-order approximation of the partial differential operators and of the solute concentration field using radial basis functions augmented with monomials. The proposed method is demonstrated on a two-dimensional \h-adaptive solution of the diffusive growth of dendrite and evaluated by comparing the results to the PFIC approach. It is shown that MIT can reproduce the results calculated with PFIC, that it is convergent and that it can capture more details in the envelope shape than PFIC with a similar spatial discretization.
    \end{abstract}

    %Research highlights
    \begin{highlights}
        \item Novel method for the solution of the Grain Envelope Model (GEM) for dendritic solidification employing sharp interface tracking built on meshless principles.

        \item The new method uses \h-adaptive spatial discretization for differential operator approximation and solute concentration field approximation on a moving boundary problem.

        \item Meshless interface tracking (MIT) 
        %is numerically more accurate than the interface capturing used in previous work because it 
        \hl{uses a conceptually different approach to determine the position of the moving interface than the interface capturing used in previous work. It thus avoids numerical artefacts linked to the use of a phase field for interface capturing.}

        \item Compared to the interface capturing method, MIT requires less computational nodes to capture the details in the dendritic envelope shape.

    \end{highlights}

    \begin{keyword}
        GEM \sep meshless \sep moving boundary \sep dendrite \sep surface reconstruction \sep RBF-FD \sep high-order approximation \sep \h-adaptivity \sep solidification \sep modeling
    \end{keyword}

\end{frontmatter}

\linenumbers
%
% Introduction
\input{introduction.tex}

%
% GEM
\input{gem.tex}

%
% Diffuse-interface
\input{diffuse.tex}

%
% Sharp-interface. Algorithm and description.
\input{sharp.tex}

%
% Results
\input{results.tex}

%
% Conclusions.
\input{conclusions.tex}



% Acknowledgments
\section*{Acknowledgments}
M.J.\ and G.K.\ acknowledge the financial support from the Slovenian Research Agency research core funding No.\ P2-0095, and research projects No.\ J2-3048 and No. \ N2 - 0275. M.Z.\ acknowledges the support by the French State through the program ``Investment in the future'' operated by the National Research Agency (ANR) and referenced by ANR-11 LABX-0008-01 (LabEx DAMAS). A part of the required high performance computing resources was provided by the EXPLOR center hosted by the Université de Lorraine.

Funded by National Science Centre, Poland under the OPUS call in the Weave programme 2021/43/I/ST3/00228.
This research was funded in whole or in part by National Science Centre (2021/43/I/ST3/00228). For the purpose of Open Access,
the author has applied a CC-BY public copyright licence to any Author Accepted Manuscript (AAM) version arising from this submission.

\section*{Declarations}
\textbf{Conflict of interest.} The authors declare that they have no conflict of interest. All the co-authors have confirmed to know the submission of the manuscript by the corresponding author.

% References
\bibliographystyle{elsarticle-num}
\bibliography{references}

\end{document}
\endinput
