import xml.etree.ElementTree as ET
import numpy as np

file_path = "../input/"
master_file = "test.xml"

# Read master file.
tree = ET.parse(file_path + master_file)
root = tree.getroot()

# Manipulated data.
dendrite_hs = np.logspace(-1, -1.7, 20)
liquid_hs = [0.1]
engines = ["rbffd"]
supports = [13]

# Manipulate XML and save.
N = 0
for dendrite_h in dendrite_hs:
    root.find("domain/dendrite").set("h", "{}".format(dendrite_h))  # Dimensionality.

    for liquid_h in liquid_hs:
        root.find("domain/liquid").set("h", "{}".format(liquid_h))  # Is uniform.

        for engine in engines:
            root.find("engine").set("type", "{}".format(engine))  # Basis type.

            for support in supports:
                root.find("engine").set("support_size", "{}".format(support))  # Monomial degree.

                root.find("meta").set('filename_out', 'results_%.6d' % N)
                tree.write('../input/generated/settings_%.6d.xml' % N)
                N += 1

print('Created ' + str(N) + ' files.')
