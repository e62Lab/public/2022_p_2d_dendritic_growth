# Raw data

Raw results from simulations, missing (large) `*.h5` files - can be obtained by running simulation or simply downloaded from zenodo: [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.8386292.svg)](https://doi.org/10.5281/zenodo.8386292).
