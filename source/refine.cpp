//
// Created by mjancic on 21/11/22.
//
#include <medusa/Medusa.hpp>
#include "helpers/SplineShape.h"

using namespace mm;
using namespace std;

template <typename vec_t>
double dx(const vec_t& p, KDTree<vec_t>& dendrite_tree, KDTree<vec_t>& liquid_tree,
          const XML& conf) {
    Range<double> distances2;
    Range<int> closest;
    auto domain_size = conf.get<double>("domain.box");
    auto dendrite_h = conf.get<double>("domain.h_min");
    auto liquid_h = conf.get<double>("domain.h_max");

    // Get 2 points closest to p on dendrite boundary.
    std::tie(closest, distances2) = dendrite_tree.query(p, 2);
    vec_t p_dendrite = dendrite_tree.get(closest[1]);  // Coordinates of the closest point.

    //    // Find point on liquid boundary in the direction of p_dendrite -> p.
    //    double a = (p - p_dendrite).dot(p - p_dendrite);
    //    double b = 2 * (p - p_dendrite).dot(p_dendrite);
    //    double c = p_dendrite.dot(p_dendrite) - domain_size * domain_size;
    //    double t1 = (-b + sqrt(b * b - 4 * a * c)) / (2 * a);
    //    double t2 = (-b - sqrt(b * b - 4 * a * c)) / (2 * a);
    //    double t = max(t1, t2);
    //
    //    vec_t p_liquid = p_dendrite + t * (p - p_dendrite);

    std::tie(closest, distances2) = liquid_tree.query(p, 2);
    vec_t p_liquid = liquid_tree.get(closest[1]);  // Coordinates of the closest point.

    double x = (p - p_dendrite).norm() / (p_liquid - p_dendrite).norm();
    return dendrite_h + (liquid_h - dendrite_h) * x;  // Linear.
    //    return dendrite_h + (liquid_h - dendrite_h) * (1.0 - cos(-x * mm::PI)) * 0.5;  // Cos.
}

int main(int argc, char* argv[]) {
    // Check for settings file.
    assert_msg(argc >= 2, "Second argument should be the XML parameter file.");

    // Timer initialization.
    Timer t;
    t.addCheckPoint("start");

    // Read input config.
    cout << "Reading params from: " << argv[1] << endl;
    const XML conf(argv[1]);

    // Create H5 file to store output.
    string output_file =
        conf.get<string>("meta.directory_out") + conf.get<string>("meta.filename_out") + ".h5";
    cout << "Creating results file: " << output_file << endl;
    HDF hdf(output_file, HDF::DESTROY);

    // Write params to results file.
    hdf.writeXML("conf", conf);
    hdf.close();

    // Domain.
    auto example_r = [](Vec<double, 1> t) {
        double r = pow(abs(cos(1.5 * t(0))), sin(3 * t(0)));
        return Vec2d(r * cos(t(0)), r * sin(t(0)));
    };

    auto der_example_r = [](Vec<double, 1> t) {
        double r = pow(abs(cos(1.5 * t(0))), sin(3 * t(0)));
        double der_r =
            (-1.5 * pow(abs(cos(1.5 * t(0))), sin(3 * t(0))) * sin(3 * t(0)) * sin(1.5 * t(0)) +
             3 * pow(abs(cos(1.5 * t(0))), sin(3 * t(0))) * cos(3 * t(0)) * cos(1.5 * t(0)) *
                 log(abs(cos(1.5 * t(0))))) /
            cos(1.5 * t(0));

        Eigen::Matrix<double, 2, 1> jm;
        jm.col(0) << der_r * cos(t(0)) - r * sin(t(0)), der_r * sin(t(0)) + r * cos(t(0));

        return jm;
    };

    // Define parametric curve's domain.
    BoxShape<Vec1d> param_bs(Vec<double, 1>{0.0}, Vec<double, 1>{2 * PI});

    UnknownShape<Vec2d> shape;
    DomainDiscretization<Vec2d> domain(shape);

    double h_min = conf.get<double>("domain.h_min");
    auto gradient_h = [=](Vec2d p) { return h_min; };

    GeneralSurfaceFill<Vec2d, Vec1d> gsf;
    domain.fill(gsf, param_bs, example_r, der_example_r, gradient_h);

    // Dendrite shape.
    SplineShape<Vec2d, 3> dendrite_shape(domain.positions(), conf.get<int>("domain.seed"));
    dendrite_shape.setInterior({0, 0}, true);
    DomainDiscretization<Vec2d> _domain_dendrite =
        dendrite_shape.discretizeBoundaryWithStep(h_min, -5);

    // Now build the outter box.
    BoxShape<Vec2d> box(-conf.get<double>("domain.box"), conf.get<double>("domain.box"));
    auto domain_full = box.discretizeBoundaryWithStep(conf.get<double>("domain.h_max"), -1);
    KDTree<Vec2d> tree_l(domain_full.positions());
    domain_full -= _domain_dendrite;

    GeneralFill<Vec2d> fill;
    fill.seed(conf.get<int>("domain.seed"));

    KDTree<Vec2d> tree(_domain_dendrite.positions());
    auto fn = [&](const Vec2d& p) { return dx(p, tree, tree_l, conf); };

    // Fill engine.
    // Fill domain with nodes.
    domain_full.fill(fill, fn);
    prn(domain_full.size());

    hdf.atomic().writeDomain("domain", domain_full);
    // Get boundaries.
    Range<int> interior = domain_full.interior();
    Range<int> dir = domain_full.types() == -5;
    Range<int> neu = domain_full.types() == -1;

    // Store to hdf.
    // For interior and Dirichlet boundary.
    domain_full.findSupport(mm::FindClosest(13).forNodes(interior + dir));
    // For Neumann boundary.
    domain_full.findSupport(mm::FindClosest(13).forNodes(neu).searchAmong(interior).forceSelf(true));

    hdf.atomic().writeInt2DArray("supports", domain_full.supports());

    // End execution.
    t.addCheckPoint("end");
    prn(t.duration("start", "end"));
    cout << "Calculations saved to: " << output_file << "." << endl;

    // Save timer.
    hdf.atomic().writeTimer("timer", t);

    return 0;
}
