#ifndef DENDRITE_CASE_H
#define DENDRITE_CASE_H

#include <list>
#include <medusa/Medusa.hpp>
#include "../helpers/helper.hpp"
#include "../helpers/vtk.hpp"
#include "../helpers/computeSupports.h"
#include "../helpers/computeShapes.h"
#include "../TipVelocity/fTipVelocity.h"
#include "../TipVelocity/fCaVo.cpp"
#include "../TipVelocity/fTipVelocity.cpp"
#include "../helpers/SplineShape.h"
#include "../helpers/timeStepHelper.h"

using namespace std;
using namespace mm;

#define EPS 1e-8

template <typename vec_t>
struct Dendrite {
  public:
    static void Solve(const XML& conf, HDF& hdf, DomainDiscretization<vec_t>& domain) {
        // Set debug value.
        bool debug = conf.get<bool>("debug.print");

        if (debug) {
            cout << "Running dendrite growth simulation ..." << endl;
        }

        // Times.
        const double dt = getTimeStep(conf);
        cout << mm::format("dt = %e", dt) << endl;
        auto simulation_time = conf.get<double>("simulation.total_time");
        auto time_steps = ceil(simulation_time / dt);

        // Engine setup.
        auto monomial_degree = conf.get<int>("engine.monomial_degree");
        using MatrixType = Eigen::Matrix<typename vec_t::scalar_t, Eigen::Dynamic, Eigen::Dynamic>;
        // RBF-FD engine.
        Polyharmonic<double, 3> ph;
        RBFFD<decltype(ph), vec_t, ScaleToClosest, Eigen::PartialPivLU<MatrixType>> appr(
            ph, Monomials<vec_t>(monomial_degree));

        // Problem.
        int N = domain.size();
        auto _ = domain.types().filter(
            [&](int i) { return (i <= conf.get<int>("domain.dendrite.type")); });
        Eigen::VectorXd tip_velocities(N), omega_deltas(N), total_boundary_displacements(_.size());
        tip_velocities.setZero();
        omega_deltas.setZero();
        total_boundary_displacements.setZero();
        Range<vec_t> stagnant_film_positions;
        Eigen::VectorXd u(N);                    // Diffusion fields.
        KDTree<vec_t> tree(domain.positions());  // For approximant.
        auto lambda = conf.get<double>("domain.liquid.k") / (conf.get<double>("domain.liquid.rho") *
                                                             conf.get<double>("domain.liquid.cp"));
        int support_size = conf.get<int>("engine.support_size");

        Range<int> interior, neu, dir;
        mm::RaggedShapeStorage<vec_t, std::tuple<mm::Lap<vec_t::dim>, mm::Der1s<vec_t::dim>>>
            storage;
        ExplicitOperators<decltype(storage)> op;
        double discretize_every = conf.get<double>("domain.discretize_every");
        if (debug) {
            prn(domain.size());
        }
        int save_every = time_steps / conf.get<int>("meta.save_count");

        // Time loop iteration.
        for (int time_step = 0; time_step <= time_steps; time_step++) {
            // Compute total simulation time.
            double time = time_step * dt;
            if (debug) {
                prn(time);
            }
            // Boolean for saving.
            bool save_to_file = ((time_step % save_every) == 0) || (time_step == time_steps);

            // Get boundaries.
            interior = domain.interior();
            dir = domain.types().filter(
                [&](int i) { return (i <= conf.get<int>("domain.dendrite.type")); });
            neu = domain.types() == conf.get<int>("domain.liquid.type");

            // Find supports.
            computeSupports(domain, support_size, neu, dir);

            // Compute shapes.
            storage = mj::computeShapes<vec_t, decltype(appr)>(domain, conf, appr);

            // Operators.
            op = storage.explicitOperators();

            // ********************
            // Concentration field.
            // ********************
            if (time_step == 0) {
                // Set initial condition.
                setInitialConditions(u, conf, dir);
            } else {
                Eigen::VectorXd u_new(u.size());
#pragma omp parallel for default(none) shared(interior, u, u_new, dt, lambda, op)
                for (long unsigned int i = 0; i < interior.size(); ++i) {
                    long unsigned int j = interior[i];
                    u_new[j] = u[j] + dt * lambda * op.lap(u, j);
                }
                // Enforce Neumann boundary condition.
                for (int i : neu) {
                    u_new[i] = op.neumann(u, i, domain.normal(i), 0.0);
                }
                // Dirichlet boundary condition.
                setDirichletBC(u_new, conf.get<double>("domain.dendrite.omega_D"), dir);

                // Copy.
                u = u_new;
            }

            // Compute normal using field gradient.
            // Current dendrite boundary.
            auto dendrite_indexes = domain.types().filter(
                [&](int i) { return (i <= conf.get<int>("domain.dendrite.type")); });
            int N_envelope = dendrite_indexes.size();
            Range<vec_t> ns(N_envelope);
            for (int j = 0; j < N_envelope; ++j) {
                vec_t grad = op.grad(u, dendrite_indexes[j]);
                ns[j] = grad.normalized();
            }

            // Save if required.
            if (save_to_file) {
                if (debug) {
                    cout << "---------" << endl;
                    cout << "Saving at time step: " << time_step << endl;
                }

                saveToFile(hdf, u, domain, conf, time_step, tip_velocities, omega_deltas,
                           stagnant_film_positions, time, ns);
            }

            // ***********************************
            // Obtain new domain and scalar field.
            // ***********************************
            if (debug) {
                cout << "Obtaining new discretization ..." << endl;
            }
            omega_deltas.resize(domain.size());
            omega_deltas.setZero();
            tip_velocities.resize(domain.size());
            tip_velocities.setZero();
            auto new_boundary = obtainNewDendriteBoundary<MatrixType, decltype(appr)>(
                domain, u, tree, conf, tip_velocities, omega_deltas, stagnant_film_positions, appr,
                ns);
            if (debug) {
                cout << "New boundary obtained, N = " << new_boundary.size() << " ... " << endl;
            }
            update_total_envelope_displacements(new_boundary, domain, dir,
                                                total_boundary_displacements);
            auto max_dendrite_tip = conf.get<double>("domain.dendrite.max_tip_position");
            Eigen::VectorXd x_coords(new_boundary.size());
            for (int i = 0; i < new_boundary.size(); i++) {
                x_coords[i] = new_boundary[i][0];
            }
            double max_x = x_coords.cwiseAbs().maxCoeff();

            if (max_x > max_dendrite_tip) {
                mm::print_red("Stopped because max tip position reached.\n");
                break;
            }
            if (tip_velocities.maxCoeff() < 0.1) {
                mm::print_red("Stopped because max tip velocity below 0.1.\n");
                break;
            }
            if (total_boundary_displacements.maxCoeff() >
                discretize_every * conf.get<double>("domain.dendrite.h")) {
                auto domain_old = domain;
                auto u_old = u;
                getNewDiscretizationFromBoundary(domain, conf, new_boundary, domain.types()[dir]);

                // Map solution to new discretization.
                if (debug) {
                    cout << "Mapping solution to new discretization ..." << endl;
                }
                u = mapSolutionToNewDiscretization<MatrixType, decltype(appr)>(
                    u_old, domain_old, domain, tree, conf, appr);
                tree.reset(domain.positions());

                dir = domain.types().filter(
                    [&](int i) { return (i <= conf.get<int>("domain.dendrite.type")); });
                total_boundary_displacements.resize(dir.size());
                total_boundary_displacements.setZero();
            } else {
                // Just move boundary and remove nodes that are too close.
                auto dendrite_indexes = domain.types().filter(
                    [&](int i) { return (i <= conf.get<int>("domain.dendrite.type")); });
                int N_envelope = dendrite_indexes.size();
                for (int i = 0; i < N_envelope; i++) {
                    int domain_idx = dendrite_indexes[i];
                    domain.pos(domain_idx) = new_boundary[i];
                }
                // Build KD-tree on interior nodes.
                KDTree<vec_t> temp_tree(domain.positions());

                // Find nodes in interior, that are too close the new boundary.
                Range<int> too_close_node_indexes;
                for (int i = 0; i < N_envelope; i++) {
                    auto new_boundary_position = new_boundary[i];

                    // Find closest 3.
                    Range<double> distances2;
                    Range<int> closest;
                    std::tie(closest, distances2) = temp_tree.query(new_boundary_position, 4);
                    for (int j = 0; j < closest.size(); ++j) {
                        if ((distances2[j] <
                             std::pow(0.8 * conf.get<double>("domain.dendrite.h"), 2)) &&
                            (domain.type(closest[j]) > 0)) {
                            too_close_node_indexes.push_back(closest[j]);
                        }
                    }
                }

                // Create list of unique indexes to be removed.
                Range<int> too_close;
                for (int i = 0; i < too_close_node_indexes.size(); i++) {
                    int j;
                    for (j = 0; j < i; j++)
                        if (too_close_node_indexes[i] == too_close_node_indexes[j]) break;
                    if (i == j) too_close.push_back(too_close_node_indexes[i]);
                }

                // Remove nodes from domain.
                if (too_close.size() != 0) {
                    domain.removeNodes(too_close);

                    Eigen::VectorXd t_u(domain.size());
                    Eigen::VectorXd t_omega(domain.size());
                    Eigen::VectorXd t_velo(domain.size());
                    tree.reset(domain.positions());
                    int c = 0;
                    for (int i = 0; i < omega_deltas.size(); ++i) {
                        bool add = true;
                        for (int j = 0; j < too_close.size(); ++j) {
                            if (i == too_close[j]) {
                                add = false;
                                break;
                            }
                        }

                        if (add) {
                            t_omega[c] = omega_deltas[i];
                            t_velo[c] = tip_velocities[i];
                            t_u[c] = u[i];
                            c++;
                        }
                    }
                    u = t_u;
                    omega_deltas = t_omega;
                    tip_velocities = t_velo;
                }
            }
        }
    };

  private:
    static void setInitialConditions(Eigen::VectorXd& u, const XML& conf, const Range<int>& dir) {
        auto liquid_temp = conf.get<double>("model.omega0");
        auto dendrite_temp = conf.get<double>("domain.dendrite.omega_D");

        cout << "Setting initial conditions..." << endl;
        u.setConstant(liquid_temp);             // Set entire domain to liquid temp.
        setDirichletBC(u, dendrite_temp, dir);  // Set dirichlet BC at dendritic envelope.
    };

    static void setDirichletBC(Eigen::VectorXd& u, double value, const Range<int>& indexes) {
        for (int i : indexes) {
            u[i] = value;
        };
    };

    static void getNewDiscretizationFromBoundary(DomainDiscretization<vec_t>& domain,
                                                 const XML& conf, Range<vec_t>& dendrite_positions,
                                                 Range<int> types) {
        bool debug = conf.get<bool>("debug.print");
        if (debug) {
            cout << "Obtaining new discretization from new boundary." << endl;
        }

        // Dendrite domain.
        auto dendrite_h = conf.get<double>("domain.dendrite.h");
        auto liquid_h = conf.get<double>("domain.liquid.h");
        auto dendrite_type = conf.get<int>("domain.dendrite.type");
        auto dendrite_xs = get_vectors(conf, "x");
        auto dendrite_ys = get_vectors(conf, "y");
        auto dendrite_fis = get_vectors(conf, "fi");
        int number_of_dendrites = dendrite_xs.size();
        auto liquid_type = conf.get<int>("domain.liquid.type");
        double liquid_size = conf.get<double>("domain.liquid.size");
        int seed = conf.get<int>("domain.fill_seed");
        if (seed == -1) {
            seed = get_seed();
        }

        // Liquid domain.
        BoxShape<Vec2d> box_liquid(-liquid_size, liquid_size);
        domain = box_liquid.discretizeBoundaryWithStep(liquid_h, liquid_type);
        if (conf.get<bool>("debug.anisotropy")) {
            BallShape<Vec2d> shape(0, liquid_size);
            domain = shape.discretizeBoundaryWithStep(liquid_h, liquid_type);
        }
        KDTree<Vec2d> test_tree(domain.positions());
        for (int i : domain.boundary()) {
            domain.type(i) = conf.get<int>("domain.liquid.type");
        }
        for (int i = 0; i < number_of_dendrites; ++i) {
            auto rng = std::default_random_engine{};
            Range<vec_t> pos_dendrite = dendrite_positions[types == dendrite_type - i];
            std::shuffle(std::begin(pos_dendrite), std::end(pos_dendrite), rng);

            SplineShape<Vec2d, 3> dendrite_shape(pos_dendrite, seed);
            dendrite_shape.setInterior({dendrite_xs[i], dendrite_ys[i]}, true);
            DomainDiscretization<Vec2d> _domain_dendrite =
                dendrite_shape.discretizeBoundaryWithStep(dendrite_h, dendrite_type - i);

            // Difference.
            domain -= _domain_dendrite;
        }

        // Remove corner nodes.
        auto borders = domain.shape().bbox();
        Range<int> corner = domain.positions().filter([&](const Vec2d& p) {
            // remove nodes that are EPS close to more than 1 border
            return (((p - borders.first) < EPS).size() + ((borders.second - p) < EPS).size()) > 1;
        });
        domain.removeNodes(corner);

        // Fill domain.
        auto dendrite_idxs = domain.types().filter([&](int i) { return (i <= dendrite_type); });
        auto spline_points = domain.positions()[dendrite_idxs];
        KDTree<vec_t> tree(spline_points);
        auto fn = [&](const vec_t& p) { return dx(p, tree, test_tree, conf); };
        // Fill engine.
        GeneralFill<vec_t> fill;
        fill.numSamples(conf.get<int>("domain.fill_samples")).seed(seed);
        // Fill domain with nodes.
        domain.fill(fill, fn);

        // Flip normals.
        for (long unsigned int i = 0; i < dendrite_idxs.size(); ++i) {
            domain.normal(dendrite_idxs[i]) = -domain.normal(dendrite_idxs[i]);
        }
    };

    template <typename matrix_t, typename approx_rbffd>
    static Eigen::VectorXd mapSolutionToNewDiscretization(Eigen::VectorXd& u_old,
                                                          DomainDiscretization<vec_t>& domain_old,
                                                          DomainDiscretization<vec_t>& domain,
                                                          KDTree<vec_t>& old_tree, const XML& conf,
                                                          approx_rbffd rbffd_approx) {
        bool debug = conf.get<bool>("debug.print");
        Eigen::VectorXd u(domain.size());

        if (debug) {
            cout << "Mapping to new discretization ..." << endl;
        }

        // Use interpolants to obtain values.
        u.resize(domain.size());

#pragma omp parallel for default(shared)
        for (int i = 0; i < domain.size(); ++i) {
            auto p = domain.pos(i);

            Range<double> distances2;  // squares of distances
            Range<int> closest;
            std::tie(closest, distances2) = old_tree.query(p, conf.get<int>("engine.support_size"));
            Range<vec_t> support = old_tree.get(closest);
            auto values = u_old(closest);

            auto appr = rbffd_approx.getApproximant(p, support, values);
            double val = appr(p);

            u(i) = val;
        }

        // Enforce Dirichlet BC.
        auto dendrite_temp = conf.get<double>("domain.dendrite.omega_D");
        auto dir = domain.types().filter(
            [&](int i) { return (i <= conf.get<int>("domain.dendrite.type")); });
        setDirichletBC(u, dendrite_temp, dir);

        return u;
    };

    template <typename matrix_t, typename rbffd_approx>
    static Range<vec_t> obtainNewDendriteBoundary(const DomainDiscretization<vec_t>& domain,
                                                  const Eigen::VectorXd& u, KDTree<vec_t>& tree,
                                                  const XML& conf, Eigen::VectorXd& tip_velocities,
                                                  Eigen::VectorXd& omega_deltas,
                                                  Range<vec_t>& stagnant_film_positions,
                                                  rbffd_approx rbffd_engine,
                                                  Range<vec_t>& grad_normals) {
        // Params.
        auto dendrite_type = conf.get<int>("domain.dendrite.type");
        auto dendrite_xs = get_vectors(conf, "x");
        auto dendrite_ys = get_vectors(conf, "y");
        auto dendrite_fis = get_vectors(conf, "fi");
        auto delta = conf.get<double>("model.delta");
        auto time_step = getTimeStep(conf);

        // Current dendrite boundary.
        Range<int> dendrite_indexes =
            domain.types().filter([&](int i) { return (i <= dendrite_type); });
        int N_envelope = dendrite_indexes.size();

        // Prepare arrays.
        auto dir = dendrite_indexes;

        // Compute new boundary.
        Range<vec_t> new_boundary(N_envelope);
        Range<vec_t> temp_stagnant_film_positions(N_envelope);

#pragma omp parallel for default(shared)
        for (int i = 0; i < N_envelope; i++) {
            // ************************************
            // Compute temperature delta positions.
            // ************************************
            int domain_idx = dendrite_indexes[i];
            auto pos = domain.pos(domain_idx);
            auto normal = domain.normal(domain_idx);
            auto node_type = domain.type(domain_idx);
            int dendrite_id = abs(node_type - dendrite_type);
            normal = -normal.normalized();  // Flip normal.
            normal = conf.template get<bool>("domain.dendrite.normals_from_shape")
                         ? normal
                         : grad_normals[i];
            //            prn(normal);
            vec_t temperature_position = pos + delta * normal;
            temp_stagnant_film_positions[i] = temperature_position;

            // ********************
            // Employ approximator.
            // ********************
            Range<double> distances2;  // squares of distances
            Range<int> closest;
            std::tie(closest, distances2) =
                tree.query(temperature_position, conf.get<int>("engine.support_size"));
            Range<vec_t> support = tree.get(closest);
            auto values = u(closest);

            auto appr = rbffd_engine.getApproximant(temperature_position, support, values);
            double temp_at_delta = appr(temperature_position);
            //            prn(temp_at_delta)

            // **********************************
            // Move boundary nodes to obtain new.
            // **********************************
            omega_deltas[domain_idx] = temp_at_delta;

            auto v_tip = getVelocity(conf, temp_at_delta);
            //            prn(v_tip)
            tip_velocities[domain_idx] = v_tip;
            // TODO:: remove the multiplication.
            auto phi = GetSmallestAngle<vec_t>(normal, dendrite_fis[dendrite_id]);
            phi *= conf.template get<bool>("debug.anisotropy") ? 0 : 1;

            vec_t disp = time_step * v_tip * cos(phi) * normal;
            vec_t new_position = pos + disp;
            //            prn(pos);
            //            prn(new_position);
            new_boundary[i] = new_position;
        }

        stagnant_film_positions = temp_stagnant_film_positions;

        //        Range<int> types = domain.types()[dendrite_indexes];
        //        Range<vec_t> old_bnd_1 = domain.positions()[domain.types() == dendrite_type];
        //        Range<vec_t> old_bnd_2 = domain.positions()[domain.types() == dendrite_type - 1];
        //        Range<vec_t> new_bnd_1 = new_boundary[types == -2];
        //        Range<vec_t> new_bnd_2 = new_boundary[types == -3];
        //        prn(old_bnd_1);
        //        prn(new_bnd_1);
        //        prn(old_bnd_2);
        //        prn(new_bnd_2);
        return new_boundary;
    };

    static void saveToFile(HDF& hdf, const Eigen::VectorXd& u,
                           const DomainDiscretization<vec_t>& domain, const XML& conf,
                           const int time_step, const Eigen::VectorXd& tip_velocities,
                           const Eigen::VectorXd& omega_deltas,
                           const Range<vec_t>& stagnant_film_positions, const double time,
                           Range<vec_t>& grad_normals) {
        cout << "Saving ..." << endl;
        hdf.setGroupName(format("step_%08d", time_step));
        hdf.atomic().writeDomain("domain", domain);
        hdf.atomic().writeDoubleArray("solution", u);
        hdf.atomic().template writeEigen("tip_velocities", tip_velocities);
        hdf.atomic().template writeEigen("omega_deltas", omega_deltas);
        hdf.atomic().writeDouble2DArray("stagnant_film", stagnant_film_positions);
        hdf.atomic().writeDoubleAttribute("time", time);
        double max_tip_velocity = *std::max_element(tip_velocities.begin(), tip_velocities.end());
        hdf.atomic().writeDoubleAttribute("max_tip_velocity", max_tip_velocity);
        hdf.atomic().template writeDouble2DArray("grad_normals", grad_normals);

        //        // Save results to VTK.
        //        std::stringstream fname;
        //        fname << conf.get<string>("meta.filename_out");
        //
        //        vtk::outputVTK vtk(domain, "Phase change simulation", fname.str().c_str(),
        //        time_step,
        //                           "../data");
        //        vtk.addScalar(u, "temperature");
        //        vtk.addScalar(domain.types(), "type");
        //        VectorField3d n(domain.size());
        //        n.setZero();
        //        for (int i : domain.boundary()) {
        //            auto nn = domain.normal(i);
        //            n[i] = Vec3d{nn[0], nn[1], 0.0};
        //        }
        //        vtk.addVector(n, "normals");
        //        vtk.close();

        cout << " Saving completed." << endl;
    };

    static double getVelocity(const XML& conf, double temp) {
        const auto omega0 = conf.get<double>("model.omega0");
        const auto delta = conf.get<double>("model.delta");

        vector<double> IvState(3);
        IvState = IvTip(omega0);
        const double VIv = IvState[0];
        const double PeIv = IvState[2];

        return fTipVelocity(temp, delta, omega0, PeIv, VIv);
    };

    static void resetRange(Range<double>& r, int N) {
        r.clear();
        Range<double> _temp(N);
        for (int i = 0; i < N; i++) {
            _temp[i] = 0.0;
        }
        r = _temp;
    }
};

#endif
