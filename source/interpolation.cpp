#include <medusa/Medusa.hpp>
#include <list>
#include "helpers/SplineShape.h"

using namespace mm;
using namespace std;

double u_analytic(Vec2d p) {
    double x = p(0), y = p(1);

    //    return ipow<2>(x * x - y - 11) + ipow<2>(x + y * y - 7);
    return std::cos(4 * mm::PI * std::sqrt(ipow<2>(x * x - 0.25) + ipow<2>(y * y - 0.25)));
}

int main(int argc, char* argv[]) {
    // Check for settings file.
    assert_msg(argc >= 2, "Second argument should be the XML parameter file.");

    // Timer initialization.
    Timer t;
    t.addCheckPoint("start");

    // Read input config.
    cout << "Reading params from: " << argv[1] << endl;
    const XML conf(argv[1]);

    // Create H5 file to store output.
    string output_file =
        conf.get<string>("meta.directory_out") + conf.get<string>("meta.filename_out") + ".h5";
    cout << "Creating results file: " << output_file << endl;
    HDF hdf(output_file, HDF::DESTROY);

    // Write params to results file.
    hdf.writeXML("conf", conf);
    hdf.close();

    double dx_fit = conf.get<double>("domain.dx_fit");
    double dx_test = conf.get<double>("domain.dx_test");

    BoxShape<Vec2d> shape(0, 1);
    auto fit_domain = shape.discretizeBoundaryWithStep(dx_fit);
    auto test_domain = shape.discretizeWithStep(dx_test);

    // Fill.
    {
        GeneralFill<Vec2d> fill;
        fill.seed(conf.get<int>("domain.seed"));

        // Fill fit points domain.
        fill(
            fit_domain, [&](const Vec2d& p) { return dx_fit; }, 1);
        prn(fit_domain.size());
        // Fill fit test domain.
        //        fill(
        //            test_domain, [&](const Vec2d& p) { return dx_test; }, 1);
        prn(test_domain.size());
    }

    hdf.atomic().writeDomain("fit_domain", fit_domain);
    hdf.atomic().writeDomain("test_domain", test_domain);

    // ******************
    // ANALYTIC SOLUTION.
    // ******************
    Eigen::VectorXd sol_ana(test_domain.size());
    for (int i : test_domain.all()) {
        auto p = test_domain.pos(i);
        sol_ana(i) = u_analytic(p);
    }
    hdf.atomic().writeEigen("analytic", sol_ana);

    // ***********
    // FIT POINTS
    // ***********
    Eigen::VectorXd sol_fit(fit_domain.size());
    for (int i : fit_domain.all()) {
        auto p = fit_domain.pos(i);
        sol_fit(i) = u_analytic(p);
    }
    hdf.atomic().writeEigen("fit_points", sol_fit);

    // *********
    // SHEPPARD
    // *********
    prn("Sheppard");
    SheppardInterpolant<Vec2d, double> interpolant;
    interpolant.setPositions(fit_domain.positions());
    {
        // Convert solution from vector to range.
        Range<double> values(sol_fit.size());
        for (int i = 0; i < sol_fit.size(); ++i) {
            values[i] = static_cast<double>(sol_fit(i));
        }
        interpolant.setValues(values);
    }

    auto sheppard_closest = split(conf.get<string>("sheppard.closest"), ",");
    for (string sheppard_cl : sheppard_closest) {
        int closest = stoi(sheppard_cl);
        Eigen::VectorXd sol_sheppard(test_domain.size());
        for (int i : test_domain.all()) {
            auto p = test_domain.pos(i);
            sol_sheppard(i) = interpolant(p, closest);
        }
        hdf.atomic().writeEigen(mm::format("Sheppard_sup_%.2d", closest), sol_sheppard);
    }

    // ***
    // PU
    // ***
    RBFFD<Polyharmonic<double>, Vec2d, ScaleToClosest> approx({3}, Monomials<Vec2d>(conf.get<int>("rbffd.mon")));
    auto support_sizes = split(conf.get<string>("rbffd.support_size"), ",");
    for (string support_size : support_sizes) {
        std::cout << "PU: sup = " << support_size << std::endl;

        int sup = stoi(support_size);

        fit_domain.findSupport(FindClosest(sup));
        // Interpolated value.
        auto sol_pu =
            PUApproximant<Vec2d>::evaluate(fit_domain, sol_fit, test_domain.positions(), 1, approx);

        hdf.atomic().writeEigen(mm::format("PU_sup_%.2d", sup), sol_pu);
    }

    // *********************
    // RBF-FD approximation
    // *********************
    prn("RBF-FD");
    KDTree<Vec2d> tree(fit_domain.positions());
    Eigen::VectorXd sol_rbffd(test_domain.size());
    support_sizes = split(conf.get<string>("rbffd.support_size"), ",");
    for (string support_size : support_sizes) {
        int sup = stoi(support_size);
        for (int i : test_domain.all()) {
            auto p = test_domain.pos(i);

            Range<double> distances2;  // squares of distances
            Range<int> closest;
            std::tie(closest, distances2) = tree.query(p, sup);
            Range<Vec2d> support = tree.get(closest);
            auto values = sol_fit(closest);

            auto appr = approx.getApproximant(p, support, values);
            double val = appr(p);

            sol_rbffd(i) = val;
        }

        hdf.atomic().writeEigen(mm::format("RBF-FD_sup_%.2d", sup), sol_rbffd);
    }

    // End execution.
    t.addCheckPoint("end");
    prn(t.duration("start", "end"));
    cout << "Calculations saved to: " << output_file << "." << endl;

    // Save timer.
    hdf.atomic().writeTimer("timer", t);

    return 0;
}
