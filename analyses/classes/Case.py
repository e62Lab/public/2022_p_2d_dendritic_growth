import json
import numpy as np
import h5py as h5


class Case:
    def __init__(self, filePath):
        self.filePath = filePath

        print('Loading data from %s ...' % self.filePath)
        self.loadFromH5()

    # Loads data from H5 file.

    def loadFromH5(self):
        # Check if file is *.h5 format.
        if (self.filePath.find(".h5") == - 1):
            print('File is not *.h5.')
            return

        # Open h5 file.
        data = h5.File(self.filePath, 'r')
        # Get groups.
        groups = list(data.keys())
        self.positions = []
        self.normals = []
        self.solutions = []
        self.types = []
        self.times = []
        self.velocities = []
        self.omega_deltas = []
        self.tip_velocities = []
        self.stagnant_film = []
        for group in groups:
            if "step" not in group:
                continue

            self.positions.append(data[group]['domain']['pos'][:])
            self.normals.append(data[group]['domain']['normals'][:])
            self.solutions.append(data[group]['solution'][:])
            self.tip_velocities.append(data[group]['tip_velocities'][:])
            self.omega_deltas.append(data[group]['omega_deltas'][:])
            # self.stagnant_film.append(data[group]['stagnant_film'][:])
            self.types.append(data[group]['domain']['types'][:])
            self.times.append(data[group].attrs['time'])
            self.velocities.append(data[group].attrs['max_tip_velocity'])

        # Load from attributes.
        # Params.
        # self.attribute_keys = list(data['conf'].attrs().keys())
        # self.attribute_values = list(data['conf'].attrs().values())
        # Timer.
        # time_keys = list(data['timer'].attrs.keys())
        # time_values = list(data['timer'].attrs.values())
        # self.timer = [[name, value]
        #               for name, value in zip(time_keys, time_values)]
        data.close()
