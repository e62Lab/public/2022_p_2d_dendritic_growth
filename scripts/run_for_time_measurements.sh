# Move to bin.
cd ../bin

# Loop over all files.
for i in 000000 000014; do
  echo "Running on input: ../input/generated/settings_${i}.xml"
  taskset -c 0 ./phase-change ../input/generated/settings_${i}.xml >>/dev/null
done

# Move back to original directory.
cd ../scripts/
