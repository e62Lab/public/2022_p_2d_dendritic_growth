# Log

## 18. 03. 2022

- Created project.
- Added files.

## 24. 03. 2022

- Updated project.
- Shape fixed so that the straight line, when growing, remains straight.
  ![](screenshots/straight_24_03_2022.png)

## 01. 04. 2022

- I made a short core scan to see the optimal value of threads. See table below. The results are for
  20 time steps with approximately computational 12500 nodes.

```txt
cores = 1, time = 76.2913
cores = 2, time = 90.4259
cores = 3, time = 70.8636
cores = 4, time = 60.828
cores = 5, time = 55.0401
cores = 6, time = 48.9757
cores = 7, time = 46.9185
cores = 8, time = 40.4387
cores = 9, time = 41.5942
cores = 10, time = 38.0811
cores = 11, time = 38.2889
cores = 12, time = 35.1795
cores = 13, time = 36.1969
cores = 14, time = 35.7703
cores = 15, time = 35.1232
cores = 16, time = 33.2132
cores = 17, time = 32.7272
cores = 18, time = 32.0432
cores = 19, time = 30.6829
cores = 20, time = 32.7994
cores = 21, time = 32.1316
cores = 22, time = 31.9224
cores = 23, time = 33.0614
cores = 24, time = 32.1574
cores = 25, time = 31.4839
cores = 26, time = 32.5481
cores = 27, time = 31.7998
cores = 28, time = 30.1866
cores = 29, time = 31.6714
cores = 30, time = 30.939
cores = 31, time = 29.8794
cores = 32, time = 30.5837
cores = 33, time = 30.7441
cores = 34, time = 31.4848
cores = 35, time = 29.9869
cores = 36, time = 31.1666
cores = 37, time = 31.1758
cores = 38, time = 29.5457
cores = 39, time = 29.3858
cores = 40, time = 30.3695
cores = 41, time = 29.5123
cores = 42, time = 30.0266
cores = 43, time = 28.8371
cores = 44, time = 30.51
cores = 45, time = 31.3499
cores = 46, time = 30.348
cores = 47, time = 29.3732
cores = 48, time = 30.2916
cores = 49, time = 29.9613
```

- Added Sheppard approximators. They don't work (for small number of closest). :) As expected.

## 29. 06. 2022

- Moved back to full dendrite envelope, due to unstable simulations on a quarter.
- Slightly adjusted PU approximator - mostly simplified to work with the same engine and on the same supports as the
  diffusion equation solution.
- Time step computed automatically.

## 27. 09. 2022

First successful convergence plots obtained.

Tip velocity

![](screenshots/convergence_27_09_2022.png)

Tip shape

![](screenshots/convergence_shape_27_09_2022.png)

Computational times

![](screenshots/times_27_09_2022.png)

Number of nodes inside domain

![](screenshots/nodes_27_09_2022.png)

## 06. 10. 2022

Obtained results where the outer boundary is a box.

![](screenshots/Figure_1.png)

![](screenshots/Figure_2.png)

![](screenshots/Figure_3.png)

![](screenshots/Figure_4.png)

![](screenshots/Figure_5.png)

## 13. 10. 2022

Studying the wave-like shape:

![](screenshots/Screenshot%20from%202022-10-13%2009-15-56.png)
![](screenshots/Screenshot%20from%202022-10-13%2009-16-10.png)

![](screenshots/wave_anim_10_13_2022.gif)
