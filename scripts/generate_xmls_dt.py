import xml.etree.ElementTree as ET
import numpy as np

file_path = "../input/"
master_file = "test.xml"

# Read master file.
tree = ET.parse(file_path + master_file)
root = tree.getroot()

# Manipulated data.
supports = [13, 31]
mons = [2, 4]
dts = np.linspace(0.05, 0.15, 10)

# Manipulate XML and save.
N = 0
for dt in dts:
    root.find("simulation").set("dt_factor", "{}".format(dt))  # dt.

    for mon, support in zip(mons, supports):
        # Monomial degree.
        root.find("engine").set("support_size", "{}".format(support))
        # Monomial degree.
        root.find("engine").set("monomial_degree", "{}".format(mon))

        root.find("meta").set('filename_out',
                              'iso_timestep_{:3f}_results_mon_{:d}'.format(dt, mon))
        tree.write('../input/generated_timestep/settings_%.6d.xml' % N)
        N += 1

print('Created ' + str(N) + ' files.')
