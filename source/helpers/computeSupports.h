//
// Created by mitja on 18/03/2022.
//

#ifndef PHASE_CHANGE_COMPUTESUPPORTS_H
#define PHASE_CHANGE_COMPUTESUPPORTS_H

#include <medusa/Medusa.hpp>

template <typename vec_t>
void computeSupports(mm::DomainDiscretization<vec_t>& domain, int support_size,
                     const mm::Range<int>& neu, const mm::Range<int>& dir,
                     bool serach_among_all = false) {
    if (serach_among_all) {
        domain.template findSupport(mm::FindClosest(support_size));
    } else {
        // For interior and Dirichlet boundary.
        domain.findSupport(mm::FindClosest(support_size).forNodes(domain.interior() + dir));
        // For Neumann boundary.
        domain.findSupport(mm::FindClosest(support_size)
                               .forNodes(neu)
                               .searchAmong(domain.interior())
                               .forceSelf(true));
    }
};

#endif  // PHASE_CHANGE_COMPUTESUPPORTS_H
