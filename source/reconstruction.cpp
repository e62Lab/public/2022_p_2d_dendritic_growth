#include <medusa/Medusa.hpp>
#include <list>
#include "helpers/SplineShape.h"

using namespace mm;
using namespace std;

int main(int argc, char* argv[]) {
    // Check for settings file.
    assert_msg(argc >= 2, "Second argument should be the XML parameter file.");

    // Timer initialization.
    Timer t;
    t.addCheckPoint("start");

    // Read input config.
    cout << "Reading params from: " << argv[1] << endl;
    const XML conf(argv[1]);

    // Create H5 file to store output.
    string output_file =
        conf.get<string>("meta.directory_out") + conf.get<string>("meta.filename_out") + ".h5";
    cout << "Creating results file: " << output_file << endl;
    HDF hdf(output_file, HDF::DESTROY);

    // Write params to results file.
    hdf.writeXML("conf", conf);
    hdf.close();

    // Domain.
    auto example_r = [](Vec<double, 1> t) {
        double r = pow(abs(cos(1.5 * t(0))), sin(3 * t(0)));
        return Vec2d(r * cos(t(0)), r * sin(t(0)));
    };

    auto der_example_r = [](Vec<double, 1> t) {
        double r = pow(abs(cos(1.5 * t(0))), sin(3 * t(0)));
        double der_r =
            (-1.5 * pow(abs(cos(1.5 * t(0))), sin(3 * t(0))) * sin(3 * t(0)) * sin(1.5 * t(0)) +
             3 * pow(abs(cos(1.5 * t(0))), sin(3 * t(0))) * cos(3 * t(0)) * cos(1.5 * t(0)) *
                 log(abs(cos(1.5 * t(0))))) /
            cos(1.5 * t(0));

        Eigen::Matrix<double, 2, 1> jm;
        jm.col(0) << der_r * cos(t(0)) - r * sin(t(0)), der_r * sin(t(0)) + r * cos(t(0));

        return jm;
    };

    // Define parametric curve's domain.
    BoxShape<Vec1d> param_bs(Vec<double, 1>{0.0}, Vec<double, 1>{2 * PI});

    UnknownShape<Vec2d> shape;
    DomainDiscretization<Vec2d> domain(shape);

    double dx = conf.get<double>("domain.h");
    auto gradient_h = [=](Vec2d p) { return dx; };

    GeneralSurfaceFill<Vec2d, Vec1d> gsf;
    domain.fill(gsf, param_bs, example_r, der_example_r, gradient_h);

    hdf.atomic().writeDomain("domain", domain);

    Range<int> ordered_idx;
    ordered_idx.push_back(0);
    KDTree<Vec2d> tree(domain.positions());
    while (ordered_idx.size() != domain.size()) {
        Vec2d last_node = domain.pos(ordered_idx[ordered_idx.size() - 1]);

        // Query for closest nodes.
        int n_neighbours = 2;
        bool found_new = false;
        while (not found_new) {
            Range<double> distances2;
            Range<int> closest;
            std::tie(closest, distances2) = tree.query(last_node, n_neighbours);

            for (int c : closest) {
                bool does_not_exist_yet = true;
                for (int ordered : ordered_idx) {
                    if (c == ordered) {
                        does_not_exist_yet = false;
                        break;
                    }
                }
                if (does_not_exist_yet) {
                    ordered_idx.push_back(c);
                    found_new = true;
                }
            }
            n_neighbours++;
        }
        prn(ordered_idx.size());
    }
    Range<Vec2d> positions_poly;
    //    for (int i : ordered_idx) {
    //        positions_poly.push_back(domain.pos(i));
    //    }
    for (int i = ordered_idx.size() - 1; i > 0; i--) {
        positions_poly.push_back(domain.pos(ordered_idx[i]));
    }

    PolygonShape<Vec2d> polygon(positions_poly);
    double dx_refine = conf.get<double>("domain.h_refine");
    DomainDiscretization<Vec2d> domain_poly = polygon.discretizeBoundaryWithStep(dx_refine);

    hdf.atomic().writeDomain("domain_poly", domain_poly);

    // Shape.

    // Shuffle dendrite points.
    auto rng = std::default_random_engine{};
    std::shuffle(std::begin(positions_poly), std::end(positions_poly), rng);

    // Dendrite shape.
    SplineShape<Vec2d> dendrite_shape(positions_poly, 15);
    dendrite_shape.setInterior({0, 0}, true);
    DomainDiscretization<Vec2d> _domain_dendrite =
        dendrite_shape.discretizeBoundaryWithStep(dx_refine, -14);

    for(int i= 0; i <_domain_dendrite.size(); i++){
        _domain_dendrite.normal(i) = -_domain_dendrite.normal(i).normalized();
    }
    hdf.atomic().writeDomain("domain_shape", _domain_dendrite);

    // End execution.
    t.addCheckPoint("end");
    prn(t.duration("start", "end"));
    cout << "Calculations saved to: " << output_file << "." << endl;

    // Save timer.
    hdf.atomic().writeTimer("timer", t);

    return 0;
}
